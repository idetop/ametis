<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.2
 * @author	hikashop.com
 * @copyright	(C) 2010-2014 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php if(hikashop_level(2) && !empty($this->extraFields['order'])){ ?>
<div  id="hikashop_checkout_additional_info" class="hikashop_checkout_additional_info">
	<div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo JText::_('ADDITIONAL_INFORMATION');?></h3>
      </div>
      <div class="panel-body">
        <table cellpadding="0" cellspacing="0" border="0" class="hikashop_contentpane">
		<?php
			if(!empty($this->extraFields['order'])){
				JRequest::setVar('hikashop_check_order',1);
				$this->setLayout('custom_fields');
				$this->type = 'order';
				echo $this->loadTemplate();
			}
		?>
		</table>
      </div>
    </div>
</div>
<div style="clear:both"></div>
<?php } ?>
