<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2014 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
$canonical=false;
if(!empty($this->element->main->product_canonical)){
	$canonical = $this->element->main->product_canonical;
}elseif(!empty($this->element->product_canonical)){
	$canonical = $this->element->product_canonical;
}
if($canonical){
	$doc = JFactory::getDocument();
	$doc->addCustomTag( '<link rel="canonical" href="'.hikashop_cleanURL($canonical).'" />' );
}


?>
<script type="text/ecmascript">


(function($){
	$(document).ready(function(){
		$("#hikashop_product_image_main a.prettyPhoto").prettyPhoto({social_tools : false, deeplinking: false});
	});	

    window.hikashopUpdateVariantData = function(selection){

			if(selection){
				var names = ['id','name','code','image','price','quantity','description','weight','url','width','length','height','contact','custom_info','files'];
				var len = names.length;
				for (var i = 0; i < len; i++){
					var el = document.getElementById('hikashop_product_'+names[i]+'_main');
					var el2 = document.getElementById('hikashop_product_'+names[i]+selection);

					if(el && el2) el.innerHTML = el2.innerHTML.replace(/_VARIANT_NAME/g, selection);
				}

				if(typeof this.window['hikashopRefreshOptionPrice'] == 'function') hikashopRefreshOptionPrice();
				if(window.Oby && window.Oby.fireAjax) window.Oby.fireAjax("hkContentChanged");
			}

			$("#hikashop_product_image_main a.prettyPhoto").prettyPhoto({social_tools : false, deeplinking: false});

			return true;
		}

})(jQuery);

</script>

<div id="hikashop_product_<?php echo @$this->element->product_code; ?>_page" class="hikashop_product_page">
<?php
$app = JFactory::getApplication();
if (empty ($this->element)) {
	$app->enqueueMessage(JText::_('PRODUCT_NOT_FOUND'));
} else {
	if(!empty($this->links->previous))
		echo "<a title='".JText :: _('PREVIOUS_PRODUCT')."' href='".$this->links->previous."'><span class='hikashop_previous_product'><i class='fa fa-chevron-left'></i></span></a>";
	if(!empty($this->links->next))
		echo "<a title='".JText :: _('NEXT_PRODUCT')."' href='".$this->links->next."'><span class='hikashop_next_product'><i class='fa fa-chevron-right'></i></span></a>";
	echo "<div class='clear_both'></div>";
	?>

	<?php
		$this->variant_name ='';
		if(!empty($this->element->variants)&&$this->config->get('variant_increase_perf',1)&&!empty($this->element->main)){
			foreach (get_object_vars($this->element->main) as $name=>$value) {
				if(!is_array($name)&&!is_object($name)){
					if(empty($this->element->$name)){
						if($name=='product_quantity' && $this->element->$name==0){
							continue;
						}
						$this->element->$name=$this->element->main->$name;
						continue;
					}

				}
				if($this->params->get('characteristic_display')=='list' && !empty ($this->element->characteristics) && !empty($this->element->main->characteristics)){
					$this->element->$name = $this->element->main->$name;
				}
			}
		}
		$this->setLayout($this->productlayout);
		echo $this->loadTemplate();

	if($this->params->get('characteristic_display')=='list'){
		$this->setLayout('show_block_characteristic');
		echo $this->loadTemplate();
	}

?>




		

	<?php  ?>
    
	<?php $contact = $this->config->get('product_contact',0); ?>
	<?php
		if(empty($this->element->variants) || $this->params->get('characteristic_display')=='list'){
			if(hikashop_level(1) && !empty($this->element->options)){
				$priceUsed = 0;
				if(!empty($this->row->prices)){
					foreach($this->row->prices as $price){
						if(isset($price->price_min_quantity) && empty($this->cart_product_price)){
							if($price->price_min_quantity<=1){
								if($this->params->get('price_with_tax')){
									$priceUsed = $price->price_value_with_tax;
								} else {
									$priceUsed = $price->price_value;
								}
							}
						}
					}
				}
				echo '
				<input type="hidden" name="hikashop_price_product" value="'.$this->row->product_id.'" />
				<input type="hidden" id="hikashop_price_product_'.$this->row->product_id.'" value="'.$priceUsed.'" />
				<input type="hidden" id="hikashop_price_product_with_options_'.$this->row->product_id.'" value="'.$priceUsed.'" />';
			}
		} else {
			$productClass = hikashop_get('class.product');
			$productClass->generateVariantData($this->element);

			$main_images =& $this->element->main->images;

			foreach ($this->element->variants as $variant) {
				$this->row = & $variant;
				$variant_name = array ();
				if (!empty ($variant->characteristics)) {
					foreach ($variant->characteristics as $k => $ch) {
						$variant_name[] = $ch->characteristic_id;
					}
				}
				$this->element->images =& $main_images;
				if (!empty ($variant->images)) {
						$this->element->images =& $variant->images;
				}

				$variant_name = implode('_', $variant_name);
				$this->variant_name = '_'.$variant_name;
				$this->setLayout('show_block_img');
				echo $this->loadTemplate();

				if(!empty($variant->product_name)){ ?>
				<div id="hikashop_product_name_<?php echo $variant_name;?>" style="display:none;">
					<?php echo $variant->product_name;?>
				</div>
				<?php }
				if ($this->config->get('show_code') &&!empty($variant->product_code)){ ?>
				<div id="hikashop_product_code_<?php echo $variant_name;?>" style="display:none;">
					<?php echo $variant->product_code;?>
				</div>
				<?php } ?>
				<div id="hikashop_product_price_<?php echo $variant_name;?>" style="display:none;">
					<?php
					if($this->params->get('show_price','-1')=='-1'){
						$config =& hikashop_config();
						$this->params->set('show_price',$config->get('show_price'));
					}
					if ($this->params->get('show_price')) {
						$this->setLayout('listing_price');
						echo $this->loadTemplate();
					}
	?>
				</div>
				<?php
				if(hikashop_level(1) && !empty($this->element->options)){
					$priceUsed = 0;
					if(!empty($this->row->prices)){
						foreach($this->row->prices as $price){
							if(isset($price->price_min_quantity) && empty($this->cart_product_price)){
								if($price->price_min_quantity<=1){
									if($this->params->get('price_with_tax')){
										$priceUsed = $price->price_value_with_tax;
									}else{
										$priceUsed = $price->price_value;
									}

								}
							}
						}
					}
					echo '
					<input type="hidden" name="hikashop_price_product" value="'.$this->row->product_id.'" />
					<input type="hidden" id="hikashop_price_product_'.$this->row->product_id.'" value="'.$priceUsed.'" />
					<input type="hidden" id="hikashop_price_product_with_options_'.$this->row->product_id.'" value="'.$priceUsed.'" />';
				}
				?>
				<div id="hikashop_product_quantity_<?php echo $variant_name;?>" style="display:none;">
					<?php

					$this->row = & $variant;
					if(empty($this->formName)) {
						$this->formName = ',0';
						if (!$this->config->get('ajax_add_to_cart', 1)) {
							$this->formName = ',\'hikashop_product_form\'';
						}
					}
					$this->ajax = 'if(hikashopCheckChangeForm(\'item\',\'hikashop_product_form\')){ return hikashopModifyQuantity(\'' . $this->row->product_id . '\',field,1' . $this->formName . ',\'cart\'); } else { return false; }';
					$this->setLayout('quantity');
					echo $this->loadTemplate();
	?>
				</div>
				<div id="hikashop_product_contact_<?php echo $variant_name;?>" style="display:none;">
				<?php

					if (hikashop_level(1) && ($contact == 2 || ($contact == 1 && !empty ($this->element->main->product_contact)))) {
						$params = @$this->params;
						global $Itemid;
						$url_itemid='';
						if(!empty($Itemid)){
							$url_itemid='&Itemid='.$Itemid;
						}
						echo $this->cart->displayButton(
							JText::_('CONTACT_US_FOR_INFO'),
							'contact_us',
							$params,
							''.hikashop_completeLink('product&task=contact&cid=' . $variant->product_id.$url_itemid),
							'window.location=\'' . hikashop_completeLink('product&task=contact&cid=' . $variant->product_id.$url_itemid) . '\';return false;'
						);
					}
	?>
				</div>
				<?php if(!empty($variant->product_description)){ ?>
				<div id="hikashop_product_description_<?php echo $variant_name;?>" style="display:none;">
					<?php echo JHTML::_('content.prepare',preg_replace('#<hr *id="system-readmore" */>#i','',$variant->product_description));?>
				</div>
				<?php
				}
				if ($this->config->get('weight_display', 0)) {

					if(!empty($variant->product_weight) && bccomp($variant->product_weight,0,3)){ ?>
					<div id="hikashop_product_weight_<?php echo $variant_name;?>" style="display:none;">
						<?php echo JText::_('PRODUCT_WEIGHT').': '.rtrim(rtrim($variant->product_weight,'0'),',.').' '.JText::_($variant->product_weight_unit); ?><br />
					</div>
					<?php
					}
				}
				if ($this->config->get('dimensions_display', 0)) {

					if (!empty ($variant->product_width) && bccomp($variant->product_width, 0, 3)) {
	?>
					<div id="hikashop_product_width_<?php echo $variant_name;?>" style="display:none;">
						<?php echo JText::_('PRODUCT_WIDTH').': '.rtrim(rtrim($variant->product_width,'0'),',.').' '.JText::_($variant->product_dimension_unit); ?><br />
					</div>
					<?php } ?>
					<?php if(!empty($variant->product_length) && bccomp($variant->product_length,0,3)){ ?>
					<div id="hikashop_product_length_<?php echo $variant_name;?>" style="display:none;">
						<?php echo JText::_('PRODUCT_LENGTH').': '.rtrim(rtrim($variant->product_length,'0'),',.').' '.JText::_($variant->product_dimension_unit); ?><br />
					</div>
					<?php } ?>
					<?php if(!empty($variant->product_height) && bccomp($variant->product_height,0,3)){ ?>
					<div id="hikashop_product_height_<?php echo $variant_name;?>" style="display:none;">
						<?php echo JText::_('PRODUCT_HEIGHT').': '.rtrim(rtrim($variant->product_height,'0'),',.').' '.JText::_($variant->product_dimension_unit); ?><br />
					</div>
					<?php

					}
				}
				if(!empty($variant->product_url)){ ?>
				<span id="hikashop_product_url_<?php echo $variant_name;?>" style="display:none;">
					<?php
					if (!empty ($variant->product_url)) {
						echo JText :: sprintf('MANUFACTURER_URL', '<a href="' . $variant->product_url . '" target="_blank">' . $variant->product_url . '</a>');
					} ?>
				</span>
				<?php } ?>
				<span id="hikashop_product_id_<?php echo $variant_name;?>">
					<input type="hidden" name="product_id" value="<?php echo $variant->product_id; ?>" />
				</span>
				<?php if(!empty($this->fields)){?>
					<div id="hikashop_product_custom_info_<?php echo $variant_name;?>" style="display:none;">
						<h4><?php echo JText::_('SPECIFICATIONS');?></h4>
						<table width="100%">
						<?php

					$this->fieldsClass->prefix = '';
					foreach ($this->fields as $fieldName => $oneExtraField) {
						if(empty($variant->$fieldName) && !empty($this->element->main->$fieldName)){
							$variant->$fieldName = $this->element->main->$fieldName;
						}
						if(!empty($variant->$fieldName)) $variant->$fieldName = trim($variant->$fieldName);
						if (!empty ($variant->$fieldName)) {
		?>
							<tr class="hikashop_product_custom_<?php echo $oneExtraField->field_namekey;?>_line">
								<td class="key">
									<span id="hikashop_product_custom_name_<?php echo $oneExtraField->field_id;?>_<?php echo $variant_name;?>" class="hikashop_product_custom_name">
										<?php echo $this->fieldsClass->getFieldName($oneExtraField);?>
									</span>
								</td>
								<td>
									<span id="hikashop_product_custom_value_<?php echo $oneExtraField->field_id;?>_<?php echo $variant_name;?>" class="hikashop_product_custom_value">
										<?php echo $this->fieldsClass->show($oneExtraField,$variant->$fieldName); ?>
									</span>
								</td>
							</tr>
						<?php

						}
					}
		?>
						</table>
					</div>
				<?php

				}
			if (!empty ($variant->files)) {
				$skip = true;
				foreach ($variant->files as $file) {
					if ($file->file_free_download)
						$skip = false;
				}
				if (!$skip) {
	?>
					<div id="hikashop_product_files_<?php echo $variant_name;?>" style="display:none;">
						<fieldset class="row hikashop_product_files_fieldset">
							<?php

					$html = array ();
					foreach ($variant->files as $file) {
						if (empty ($file->file_name)) {
							$file->file_name = $file->file_path;
						}
						$fileHtml = '';
						if (!empty ($file->file_free_download)) {
							$fileHtml = '<div class="col-md-6 hikashop_product_file_div"><a class="hikashop_product_file_link" href="' . hikashop_completeLink('product&task=download&file_id=' . $file->file_id) . '"><i class="fa fa-download"></i>' . $file->file_name . '</a></div>';
						}
						$html[] = $fileHtml;
					}
					echo implode('', $html);
	?>
						</fieldset>
					</div>
			<?php

				}
			}
		}
	}
	$this->params->set('show_price_weight', 0);
	?>
		<div class="hikashop_submodules titleCenter" id="hikashop_submodules" style="clear:both">
			<?php

	if (!empty ($this->modules) && is_array($this->modules)) {
		jimport('joomla.application.module.helper');
		foreach ($this->modules as $module) {
			echo JModuleHelper :: renderModule($module);
		}
	}
	?>
		</div>
		<?php

}
?>
</div>
<div class="tochosen"></div>