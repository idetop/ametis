<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2014 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$database = JFactory::getDBO();
?><div id="hikashop_product_top_part" class="hikashop_product_top_part clearfix">
<?php if (!empty($this->element->extraData->topBegin)) {
    echo implode("\r\n", $this->element->extraData->topBegin);
} ?>

<?php if (!empty($this->element->extraData->topEnd)) {
    echo implode("\r\n", $this->element->extraData->topEnd);
} ?>

</div>



<div class="shop shop-single">
    <div class="row">

        <div class="col-sm-5">

            <?php
            if (!empty($this->element->extraData->leftBegin)) {
                echo implode("\r\n", $this->element->extraData->leftBegin);
            }

            $this->row = & $this->element;
            $this->setLayout('show_block_img');
            echo $this->loadTemplate();

            if (!empty($this->element->extraData->leftEnd)) {
                echo implode("\r\n", $this->element->extraData->leftEnd);
            }
            ?>

        </div> <!-- and shop-content-item-container -->

        <div class="col-sm-7">

            <form class="form_shop_detail" action="<?php echo hikashop_completeLink('product&task=updatecart'); ?>" method="post" name="hikashop_product_form" enctype="multipart/form-data">                    
                <h1 >
                    <span id="hikashop_product_name_main" class="hikashop_product_name_main">
<?php
if (hikashop_getCID('product_id') != $this->element->product_id && isset($this->element->main->product_name))
    echo $this->element->main->product_name;
else
    echo $this->element->product_name;
?>
                    </span>

                </h1>






                <!-- Vote  -->
                <div id="hikashop_product_vote_mini" class="rating hikashop_product_vote_mini">
                    <?php
                    $config = & hikashop_config();
                    if ($this->params->get('show_vote_product') == '-1') {
                        $this->params->set('show_vote_product', $config->get('show_vote_product'));
                    }
                    if ($this->params->get('show_vote_product')) {
                        $js = '';
                        $this->params->set('vote_type', 'product');
                        if (isset($this->element->main)) {
                            $product_id = $this->element->main->product_id;
                        } else {
                            $product_id = $this->element->product_id;
                        }
                        $this->params->set('vote_ref_id', $product_id);
                        echo hikashop_getLayout('vote', 'mini', $this->params, $js);
                    }
                    ?>

                </div>
                <!-- End vote -->

                <!-- Price  -->
                    <?php
                    if (!empty($this->element->extraData->rightBegin))
                        echo implode("\r\n", $this->element->extraData->rightBegin);
                    ?>
                <span id="hikashop_product_price_main" class="price">
<?php
if ($this->params->get('show_price')) {
    $this->row = & $this->element;
    $this->setLayout('listing_price');
    echo $this->loadTemplate();
}
?>
                </span>
                <!-- End price -->                        

                <!-- Description  -->
                    <?php
                    if (!empty($this->element->extraData->bottomBegin))
                        echo implode("\r\n", $this->element->extraData->bottomBegin);
                    ?>


                <!-- characteristic  -->
                <div class="selectbox">
<?php
if ($this->params->get('characteristic_display') != 'list') {
    $this->setLayout('show_block_characteristic');
    echo $this->loadTemplate();
    ?>
                    </div>
                    <!-- <a href="#" class="clear-selection"><?php //echo JText::_('TPL_CLEAR_SELECTION'); ?></a> -->
                    <!-- End Characteristic -->

                    <!-- Option   -->
                        <?php
                        $form = ',0';
                        if (!$this->config->get('ajax_add_to_cart', 1)) {
                            $form = ',\'hikashop_product_form\'';
                        }
                        if (hikashop_level(1) && !empty($this->element->options)) {
                            ?>
                        <div id="hikashop_product_options" class="hikashop_product_options">
                        <?php
                        $this->setLayout('option');
                        echo $this->loadTemplate();
                        ?>
                        </div>

                        <?php
                        $form = ',\'hikashop_product_form\'';
                        if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
                            ?>
                            <input type="hidden" name="popup" value="1"/>
                            <?php
                        }
                    }

                    if (!$this->params->get('catalogue') && ($this->config->get('display_add_to_cart_for_free_products') || !empty($this->element->prices))) {
                        if (!empty($this->itemFields)) {
                            $form = ',\'hikashop_product_form\'';
                            if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
                                ?>
                                <input type="hidden" name="popup" value="1"/>
                                <?php
                            }
                            $this->setLayout('show_block_custom_item');
                            echo $this->loadTemplate();
                        }
                    }

                    $this->formName = $form;
                    if ($this->params->get('show_price')) {
                        ?>
                        <span id="hikashop_product_price_with_options_main" class="hikashop_product_price_with_options_main">
                        </span>
    <?php } ?>
                    <!-- End Option -->
                    <!-- Quantity -->
                    <div class="clearfix shop-custom-detail">

                            <?php
                            $contact = $this->config->get('product_contact', 0);
                            if (hikashop_level(1) && ($contact == 2 || ($contact == 1 && !empty($this->element->product_contact)))) {
                                ?>

                            <div id="hikashop_product_contact_main" class="btnOverflow btn btn-grey icon-pencil2" >

        <?php
        $empty = '';
        $params = new HikaParameter($empty);
        global $Itemid;
        $url_itemid = '';
        if (!empty($Itemid)) {
            $url_itemid = '&Itemid=' . $Itemid;
        }
        echo $this->cart->displayButton(JText :: _('CONTACT_US_FOR_INFO'), 'contact_us', $params, hikashop_completeLink('product&task=contact&cid=' . $this->element->product_id . $url_itemid), 'window.location=\'' . hikashop_completeLink('product&task=contact&cid=' . $this->element->product_id . $url_itemid) . '\';return false;');
        ?>


                            </div>
                                <?php
                            }
                            ?>

    <?php if (empty($this->element->characteristics) || $this->params->get('characteristic_display') != 'list') { ?>

                            <div id="hikashop_product_quantity_main" class="hikashop_product_quantity_main quantity">
        <?php
        $this->row = & $this->element;
        $this->ajax = 'if(hikashopCheckChangeForm(\'item\',\'hikashop_product_form\')){ return hikashopModifyQuantity(\'' . $this->row->product_id . '\',field,1' . $form . ',\'cart\'); } else { return false; }';
        $this->setLayout('quantity');
        echo $this->loadTemplate();
        ?>
                            </div>

    <?php } ?>


                    </div>
                    <!-- End Quantity -->
                    <br />

                            <?php if ($this->config->get('show_code')) { ?>
                        <!-- SKU  -->				
                        <div class="clearfix gray-italic skuproduct">
                            <span class="margin-reset"><?php echo JText::_('TPL_SKU'); ?> :</span>

                            <span id="hikashop_product_code_main" >
                        <?php if (strlen($this->element->product_code) > 30) { ?>
                                    <abbr title="<?php echo $this->element->product_code; ?>" style="border:none">
            <?php echo substr($this->element->product_code, 0, 30) ?>
                                    </abbr>
        <?php } else echo $this->element->product_code; ?>

                            </span>

                        </div>
                    <?php } ?>
                    <!-- End SKU -->                        




                    <?php
                    if ($this->element->product_parent_id) {
                        $tag_product_id = $this->element->product_parent_id;
                    } else {
                        $tag_product_id = $this->element->product_id;
                    }
                    $database = JFactory::getDBO();
                    $tags = array();
                    $filters = array('a.product_id=' . $tag_product_id);
                    hikashop_addACLFilters($filters, 'product_access', 'a');
                    $query = 'SELECT a.*, b.product_category_id, b.category_id, b.ordering FROM ' . hikashop_table('product') . ' AS a LEFT JOIN ' . hikashop_table('product_category') . ' AS b ON a.product_id = b.product_id WHERE ' . implode(' AND ', $filters) . ' LIMIT 1';
                    $database->setQuery($query);
                    $tag_product_element = $database->loadObject();
                    if (!empty($tag_product_element)) {
                        $tagsHelper = hikashop_get('helper.tags');
                        $tags = $tagsHelper->loadTags('product', $tag_product_element);
                    }
                    if ($tags) {
                        ?>
                        <div class="clearfix gray-italic">
                            <span class=" margin-reset"><?php echo JText::_('TPL_ITEM_PRODUCT_TAGS'); ?> :</span>
                            <?php
                            $tag_lists = array();
                            foreach ($tags as $tag) {
                                $tag_lists[] = $tag->tag_id;
                            }
                            if ($tag_lists) {
                                $database->setQuery('SELECT title FROM #__tags WHERE id IN (' . implode(',', $tag_lists) . ') ORDER BY FIND_IN_SET(id, ' . $database->quote(implode(',', $tag_lists)) . ')');
                                if ($tags = $database->loadColumn()) {
                                    foreach ($tags as $tag) {
                                        $tag = '<span class="pull-left margin-reset">' . $tag . '</span>';
                                    }
                                    echo implode(', ', $tags);
                                }
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>





                    <?php
                    $q = 'SELECT c.* ' .
                            ' FROM ' . hikashop_table('product_category') . ' as pc ' .
                            ' INNER JOIN ' . hikashop_table('category') . ' AS c ON pc.category_id = c.category_id ' .
                            ' WHERE pc.product_id = ' . hikashop_getCID('product_id');
                    $database->setQuery($q);
                    $product_categories = $database->loadObjectList();
                    $categories = array();
                    if ($product_categories) {
                        ?>
                        <div class=" gray-italic">
                            <span class=" margin-reset"><?php echo JText::_('TPL_ITEM_PRODUCT_CATEGORIES'); ?> :</span>
                        <?php
                        foreach ($product_categories as $category)
                            $categories[] = $category->category_name;
                        foreach ($categories as $c)
                            $c = '<span class="pull-left margin-reset">' . $c . '</span>';
                        echo implode(', ', $categories);
                        ?>
                        </div>
                        <?php
                    }
                    ?>
                    <!-- end -->
                    <br /> <br />



                    <span id="hikashop_product_id_main" class="hikashop_product_id_main">
                        <input type="hidden" name="product_id" value="<?php echo $this->element->product_id; ?>" />
                    </span>
                    <?php
                    if (!empty($this->element->extraData->rightEnd))
                        echo implode("\r\n", $this->element->extraData->rightEnd);
                }
                ?>

<?php
$pluginsClass = hikashop_get('class.plugins');
$plugin = $pluginsClass->getByName('content', 'hikashopsocial');
if (!empty($plugin) && (@$plugin->published || @ $plugin->enabled)) {
    echo '{hikashop_social}';
}
?>

            <?php if ($this->productlayout != 'show_tabular') { ?>
                    <input type="hidden" name="cart_type" id="type" value="cart"/>
                    <input type="hidden" name="add" value="1"/>
                    <input type="hidden" name="ctrl" value="product"/>
                    <input type="hidden" name="task" value="updatecart"/>
                    <input type="hidden" name="return_url" value="<?php echo urlencode(base64_encode(urldecode($this->redirect_url))); ?>"/>
                </form>
    <?php }
?>


        </div>
        <div class="clear"></div>
        
                                <?php
                                if ($this->productlayout != 'show_tabular') {
                                    ?>

                <div id="shopaccordion" class="panel-group about-sale-item">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapseOne" data-parent="#shopaccordion" data-toggle="collapse" class="collapsed"><?php echo JText::_('TPL_ITEM_PRODUCT_DESCRIPTION'); ?></a></h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div id="hikashop_product_description_main" class="hikashop_product_description_main">
                                <?php
                                echo JHTML::_('content.prepare', preg_replace('#<hr *id="system-readmore" */>#i', '', $this->element->product_description));
                                ?>
                                </div>


                    <?php
                    $this->setLayout('show_block_dimensions');
                    echo $this->loadTemplate();
                    ?>

                            </div>
                        </div>
                    </div>

                                <?php if ($this->params->get('show_vote_product') == 1) { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#collapseTwo" data-parent="#shopaccordion" data-toggle="collapse"><?php echo JText::_('TPL_ITEM_PRODUCT_REVIEWS'); ?></a></h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                            <?php
                                            $config = & hikashop_config();
                                            if ($config->get('enable_status_vote') == "comment" || $config->get('enable_status_vote') == "two" || $config->get('enable_status_vote') == "both") {
                                                ?>
                                        <form action="<?php echo hikashop_currentURL() ?>" method="post" name="hikashop_comment_form" id="hikashop_comment_form">
                                            <div id="hikashop_product_vote_listing" class="hikashop_product_vote_listing">
                                                <?php
                                                if ($this->params->get('show_vote_product')) {
                                                    $js = '';
                                                    if (isset($this->element->main)) {
                                                        $product_id = $this->element->main->product_id;
                                                    } else {
                                                        $product_id = $this->element->product_id;
                                                    }
                                                    $this->params->set('product_id', $product_id);
                                                    echo hikashop_getLayout('vote', 'listing', $this->params, $js);
                                                    ?>
                                                </div>
                                                <div id="hikashop_product_vote_form" class="hikashop_product_vote_form">
                                                    <?php
                                                    $js = '';
                                                    if (isset($this->element->main)) {
                                                        $product_id = $this->element->main->product_id;
                                                    } else {
                                                        $product_id = $this->element->product_id;
                                                    }
                                                    $this->params->set('product_id', $product_id);
                                                    echo hikashop_getLayout('vote', 'form', $this->params, $js);
                                                }
                                                ?>
                                            </div>
                                            <input type="hidden" name="add" value="1"/>
                                            <input type="hidden" name="ctrl" value="product"/>
                                            <input type="hidden" name="task" value="show"/>
                                            <input type="hidden" name="return_url" value="<?php echo urlencode(base64_encode(urldecode($this->redirect_url))); ?>"/>
                                        </form>
                                        <?php } ?>

                                    <div class="hikashop_external_comments" id="hikashop_external_comments" style="clear:both">
                                        <?php
                                        $config = & hikashop_config();
                                        if ($config->get('comments_feature') == 'jcomments') {
                                            $comments = HIKASHOP_ROOT . 'components' . DS . 'com_jcomments' . DS . 'jcomments.php';
                                            if (file_exists($comments)) {
                                                require_once ($comments);
                                                if (hikashop_getCID('product_id') != $this->element->product_id && isset($this->element->main->product_name)) {
                                                    $product_id = $this->element->main->product_id;
                                                    $product_name = $this->element->main->product_name;
                                                } else {
                                                    $product_id = $this->element->product_id;
                                                    $product_name = $this->element->product_name;
                                                }
                                                echo JComments::showComments($product_id, 'com_hikashop', $product_name);
                                            }
                                        } elseif ($config->get('comments_feature') == 'jomcomment') {
                                            $comments = HIKASHOP_ROOT . 'plugins' . DS . 'content' . DS . 'jom_comment_bot.php';
                                            if (file_exists($comments)) {
                                                require_once ($comments);
                                                if (hikashop_getCID('product_id') != $this->element->product_id && isset($this->element->main->product_name))
                                                    $product_id = $this->element->main->product_id;
                                                else
                                                    $product_id = $this->element->product_id;
                                                echo jomcomment($product_id, 'com_hikashop');
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
    <?php } ?>


                                <?php if (!empty($this->element->files)) { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#collapseThree" data-parent="#shopaccordion" data-toggle="collapse"><?php echo JText::_('DOWNLOADS'); ?></a></h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                        <?php
                        $this->setLayout('show_block_product_files');
                        echo $this->loadTemplate();
                        ?>
                                </div>
                            </div>
                        </div>
    <?php } ?> 


                                <?php if (!empty($this->fields)) { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#collapseFour" data-parent="#shopaccordion" data-toggle="collapse"><?php echo JText::_('TPL_ITEM_PRODUCT_ADDITIONAL'); ?></a></h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                        <?php
                        $this->setLayout('show_block_custom_main');
                        echo $this->loadTemplate();
                        ?>
                                </div>
                            </div>
                        </div>
    <?php } ?> 


                </div>

<?php } ?>
        
        
    </div>
</div>
<!-- End shop -->


<div id="hikashop_product_bottom_part" class="hikashop_product_bottom_part">

    <span id="hikashop_product_url_main" class="hikashop_product_url_main">
    <?php
    if (!empty($this->element->product_url)) {
        echo JText :: sprintf('MANUFACTURER_URL', '<a href="' . $this->element->product_url . '" target="_blank">' . $this->element->product_url . '</a>');
    }
    ?>
    </span>
    <?php
    $this->setLayout('show_block_product_files');
    // echo $this->loadTemplate();
    ?>
    <?php
    if (!empty($this->element->extraData->bottomMiddle))
    //echo implode("\r\n",$this->element->extraData->bottomMiddle);
        
        ?>
<?php
if (!empty($this->element->extraData->bottomEnd))
//echo implode("\r\n",$this->element->extraData->bottomEnd);
    
    ?>
</div>
