<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2014 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
if (!empty($this->rows)) {
    $current_column = 1;
    ?>
    <div class="module_products_list_slider owl-carousel gridItem" >


        <?php
        foreach ($this->rows as $row) {
            $this->row = & $row;
            $height = $this->params->get('image_height');
            $width = $this->params->get('image_width');
            if (empty($height))
                $height = $this->config->get('thumbnail_y');
            if (empty($width))
                $width = $this->config->get('thumbnail_x');
            $divWidth = $width;
            $divHeight = $height;
            $this->image->checkSize($divWidth, $divHeight, $row);
            $link = hikashop_completeLink('product&task=show&cid=' . $this->row->product_id . '&name=' . $this->row->alias . $this->itemid . $this->category_pathway);
            ?>
            <div class="item item<?php echo $current_column; ?>">
                <div class="innerItem">

                    <?php
                    $this->row = & $row;
                    //$this->setLayout('listing_'.$this->params->get('div_item_layout_type'));
                    $this->setLayout('listing_img_title');
                    echo $this->loadTemplate();
                    ?>


                </div>
            </div>
        <?php
        $current_column++;
    }
    ?>

    </div>
<?php } ?>
