<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.2
 * @author	hikashop.com
 * @copyright	(C) 2010-2014 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
if(isset($this->element->main)){
	if($this->element->product_weight==0 && isset($this->element->main->product_weight)){
		$this->element->product_weight = $this->element->main->product_weight;
	}
	if($this->element->product_width==0 && isset($this->element->main->product_width)){
		$this->element->product_width = $this->element->main->product_width;
	}
	if($this->element->product_height==0 && isset($this->element->main->product_height)){
		$this->element->product_height = $this->element->main->product_height;
	}
	if($this->element->product_length==0 && isset($this->element->main->product_length)){
		$this->element->product_length = $this->element->main->product_length;
	}
}
?>
<table class="description">
	<tbody>
<?php
if ($this->config->get('weight_display', 0)) {
	if(isset($this->element->product_weight) && bccomp($this->element->product_weight,0,3)){ ?>
		<tr id="hikashop_product_weight_main" class="hikashop_product_weight_main">
			<?php echo '<th>'.JText::_('PRODUCT_WEIGHT').':</th><td>'.rtrim(rtrim($this->element->product_weight,'0'),',.').' '.JText::_($this->element->product_weight_unit); ?></td>
		</tr>
	<?php
	}
}

if ($this->config->get('dimensions_display', 0) && bccomp($this->element->product_width, 0, 3)) {
?>
	<tr id="hikashop_product_width_main" class="hikashop_product_width_main gray">
		<?php echo '<th>'.JText::_('PRODUCT_WIDTH').':</th><td>'.rtrim(rtrim($this->element->product_width,'0'),',.').' '.JText::_($this->element->product_dimension_unit); ?></td>
	</tr>
<?php
}
if ($this->config->get('dimensions_display', 0) && bccomp($this->element->product_length, 0, 3)) {
?>
	<tr id="hikashop_product_length_main" class="hikashop_product_length_main ">
		<?php echo '<th>'.JText::_('PRODUCT_LENGTH').':</th><td>'.rtrim(rtrim($this->element->product_length,'0'),',.').' '.JText::_($this->element->product_dimension_unit); ?></td>
	</tr>
<?php
}
if ($this->config->get('dimensions_display', 0) && bccomp($this->element->product_height, 0, 3)) {
?>
	<tr id="hikashop_product_height_main" class="hikashop_product_height_main gray">
		<?php echo '<th>'.JText::_('PRODUCT_HEIGHT').':</th><td>'.rtrim(rtrim($this->element->product_height,'0'),',.').' '.JText::_($this->element->product_dimension_unit); ?></td>
	</tr>
<?php
}

?>
</tbody>
</table>
<?php
