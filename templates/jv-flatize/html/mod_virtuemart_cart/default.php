<?php // no direct access
defined('_JEXEC') or die('Restricted access');

//dump ($cart,'mod cart');
// Ajax is displayed in vm_cart_products
// ALL THE DISPLAY IS Done by Ajax using "hiddencontainer" 

if($cart->cartProductsData){
    $productModel = VmModel::getModel('Product');
    $productIds = array();
    foreach($cart->cartProductsData as &$productData){
        $productIds[] = $productData['virtuemart_product_id'];
    }
    $products = $productModel->getProducts($productIds);
    $productModel->addImages($products);
}
?>

<!-- Virtuemart 2 Ajax Card -->

<div class="vmCartModule <?php echo $params->get('moduleclass_sfx'); ?>" id="vmCartModule">
  <?php
if ($show_product_list) {
	?>
  <div id="hiddencontainer" style=" display: none; ">
    <ul class="vmcontainer ajax-vmcontainer " >
      <li class="product_row clearfix shop-card-products ">
        <div class="blogThumbnail"> </div>
        <div class="ItemBody shop-card-products-features"> <span class="moduleItemTitle product_name"></span>
          <div class="chart-custom"> <span> <?php echo JText::_('TPL_QUANTITY'); ?> : </span> &nbsp; <span class="quantity value"> </span> </div>
          <div class="customProductData chart-custom"></div>
          <div class=" chart-custom price" > <?php echo JText::_('TPL_PRICE_WITH_TAX'); ?>:  &nbsp; <span class="subtotal_with_tax value"></span></div>
        </div>
      </li>
    </ul>
  </div>
  
<ul class=" top-bar-nav-shop-card dropdownMenu">
    <li> <span class="view_cart_link "> <i class="fa fa-shopping-cart"></i>  <span class="total_products"> <?php echo  $data->totalProduct;?> </span> </span>
    
        <div class="divsubmenu ModuleMiniSidebar mini-sidebar  moduleMiniCart <?php if(!$productIds) echo 'empty'?>"   >
        

<div class="module-topcart">        
            <div class="total pull-left" >
              <?php if ($data->totalProduct and $show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
              <span><?php echo $data->billTotal; ?></span>
              <?php } ?>
            </div>
            <div class="show_cart cartProceed">
              <?php if ($data->totalProduct) echo  $data->cart_show; ?>
            </div>
</div>
        
        	<div class="vm_cart_products">
            <ul class="vmcontainer" >
              <?php
                    foreach ($data->products as $k=>$product){
                        ?>
              <li class="product_row clearfix shop-card-products ">
                <div class="blogThumbnail">
                  <?php
				    //if($products) foreach($products as $p){
                        //if($product['product_sku'] == $p->product_sku){
                            if (!empty($products[$k]->images[0])) {
                                echo $products[$k]->images[0]->displayMediaThumb ('', FALSE);
                            }
                        //}
                    //}
                    ?>
                </div>
                <div class="ItemBody shop-card-products-features"> <span class="moduleItemTitle product_name"><?php echo  $product['product_name'] ?></span>
                  <div class="chart-custom"> <span> <?php echo JText::_('TPL_QUANTITY'); ?> : </span> &nbsp; <span class="quantity value"> <?php echo  $product['quantity'] ?></span> </div>
                  <?php if ( !empty($product['customProductData']) ) { ?>
                  <div class="customProductData chart-custom"><?php echo $product['customProductData'] ?></div>
                  <?php } ?>
                  <?php if ($show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                  <div class=" chart-custom price" > <?php echo JText::_('TPL_PRICE_WITH_TAX'); ?>:  &nbsp; <span class="subtotal_with_tax value"><?php echo $product['subtotal_with_tax'] ?></span></div>
                  <?php } ?>
                </div>
              </li>
              <?php }
                ?>
            </ul>
          </div>



            

            <div class="payments_signin_button" ></div>
            <noscript>
            <?php echo vmText::_('MOD_VIRTUEMART_CART_AJAX_CART_PLZ_JAVASCRIPT') ?>
            </noscript>  
    
        </div>
    </li>
</ul>
  
  
  
  <?php } ?>

</div>
<script type="text/javascript">
    (function (jQuery) {
        jQuery.fn.updateVirtueMartCartModule = function (arg) {

            var options = jQuery.extend({}, jQuery.fn.updateVirtueMartCartModule.defaults, arg);

            return this.each(function () {

                // Local Variables
                var $this = jQuery(this);

                jQuery.ajaxSetup({ cache: false })
                jQuery.getJSON(window.vmSiteurl + "index.php?option=com_virtuemart&nosef=1&view=cart&task=viewJS&format=json" + window.vmLang,
                    function (datas, textStatus) {
                        if (datas.totalProduct > 0) {
                            jQuery('.moduleMiniCart').removeClass('empty');
                            jQuery.getJSON(window.vmSiteurl + "index.php?plugin=jvajax_shop_search&task=getProductImagesFromCart", function(productImages){
                                $this.find(".vm_cart_products").html("");
                                jQuery.each(datas.products, function (key, val) {
                                    if(productImages) jQuery.each(productImages, function(i, productImage){
                                        if(key == i){//if(productImage.product_sku == val.product_sku){										
                                            val.blogThumbnail = productImage.imageHtml;
                                        }
                                    });
                            		jQuery("#hiddencontainer .vmcontainer").clone().appendTo(".vmCartModule .vm_cart_products");
                                    jQuery.each(val, function (key, val) {
                                        if (jQuery("#hiddencontainer .vmcontainer ." + key)) $this.find(".vm_cart_products ." + key + ":last").html(val);
                                    });
                                });
                            });
                        }
                        $this.find(".show_cart").html(datas.cart_show);
                        $this.find(".total_products").html(datas.totalProduct);
                        $this.find(".total").html(datas.billTotal);
                    }
                );
            });
        };
    })(jQuery);
</script> 
