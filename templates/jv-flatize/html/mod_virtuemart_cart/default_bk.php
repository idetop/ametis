<?php // no direct access
defined('_JEXEC') or die('Restricted access');

//dump ($cart,'mod cart');
// Ajax is displayed in vm_cart_products
// ALL THE DISPLAY IS Done by Ajax using "hiddencontainer"

if($cart->cartProductsData){
    $productModel = VmModel::getModel('Product');
    $productIds = array();
    foreach($cart->cartProductsData as &$productData){
        $productIds[] = $productData['virtuemart_product_id'];
    }
    $products = $productModel->getProducts($productIds);
    $productModel->addImages($products);
}
?>

<!-- Virtuemart 2 Ajax Card -->
<div class="vmCartModule <?php echo $params->get('moduleclass_sfx'); ?>" id="vmCartModule">
    <?php
    if ($show_product_list) {
        ?>
        <ul id="hiddencontainer" style=" display: none; ">
            <li class="product_row">


                <div class="blogThumbnail"></div>

                <div class="ItemBody shop-card-products-features">
                    <span class="moduleItemTitle product_name"></span>

                    <div class="chart-custom"> <span> <?php echo JText::_('TPL_QUANTITY'); ?> : </span> &nbsp; <span class="quantity value"></span> </div>

                    <div class="customProductData  chart-custom"></div>

                    <?php if ($show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                        <div class="price chart-custom" >  <?php echo JText::_('TPL_PRICE_WITH_TAX'); ?>:  &nbsp;  <span class="subtotal_with_tax value"></span></div>
                    <?php } ?>


                </div>

            </li>


        </ul>
        <ul class=" top-bar-nav-shop-card dropdownMenu">
            <li> <a href="index.php?option=com_virtuemart&view=cart&Itemid=383" class="view_cart_link "> <i class="fa fa-shopping-cart"></i>  <span class="total_products"> <?php echo  $data->totalProductTxt;?> </span> </a>

                <div class="divsubmenu ModuleMiniSidebar mini-sidebar  moduleMiniCart "  >
                    <ul class="vm_cart_products">
                        <?php
                        foreach ($data->products as $product){
                            ?>
                            <li class="product_row clearfix shop-card-products ">

                                <div class="blogThumbnail">
                                    <?php
                                    if($products) foreach($products as $p){
                                        if($product['product_sku'] == $p->product_sku){
                                            if (!empty($p->images[0])) {
                                                echo $p->images[0]->displayMediaThumb ('', FALSE);
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="ItemBody shop-card-products-features">

                                    <span class="moduleItemTitle"><?php echo  $product['product_name'] ?></span>
                                    <div class="chart-custom"> <span> <?php echo JText::_('TPL_QUANTITY'); ?> : </span> &nbsp; <span class="quantity value"> <?php echo  $product['quantity'] ?></span> </div>
                                    <?php if ( !empty($product['customProductData']) ) { ?>
                                        <div class="customProductData chart-custom"><?php echo $product['customProductData'] ?></div>
                                    <?php } ?>
                                    <?php if ($show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                                        <div class=" chart-custom price" >  <?php echo JText::_('TPL_PRICE_WITH_TAX'); ?>:  &nbsp;  <span class="subtotal_with_tax value"><?php echo $product['subtotal_with_tax'] ?></span></div>
                                    <?php } ?>
                                </div>
                            </li>
                        <?php }
                        ?>
                    </ul>
                    <div class="cart-subtotal" >
                        <?php if ($data->totalProduct and $show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                            <span class="total"><?php echo $data->billTotal; ?></span>
                        <?php } ?>
                    </div>
                    
                    <div class="cartProceed">
                        <?php if ($data->totalProduct) echo  $data->cart_show; ?>
                    </div>
                    <div class="payments_signin_button" ></div>
                    <noscript>
                        <?php echo vmText::_('MOD_VIRTUEMART_CART_AJAX_CART_PLZ_JAVASCRIPT') ?>
                    </noscript>
                </div>
            </li>
        </ul>

    <?php } ?>








</div>

<script type="text/javascript">
    (function (jQuery) {
        jQuery.fn.updateVirtueMartCartModule = function (arg) {

            var options = jQuery.extend({}, jQuery.fn.updateVirtueMartCartModule.defaults, arg);

            return this.each(function () {

                // Local Variables
                var $this = jQuery(this);

                jQuery.ajaxSetup({ cache: false })
                jQuery.getJSON(window.vmSiteurl + "index.php?option=com_virtuemart&nosef=1&view=cart&task=viewJS&format=json" + window.vmLang,
                    function (datas, textStatus) {
                        if (datas.totalProduct > 0) {
                            jQuery.getJSON(window.vmSiteurl + "index.php?plugin=jvajax_shop_search&task=getProductImagesFromCart", function(productImages){
                                $this.find(".vm_cart_products").html("");
                                jQuery.each(datas.products, function (key, val) {
                                    if(productImages) jQuery.each(productImages, function(i, productImage){
                                        if(productImage.product_sku == val.product_sku){
                                            val.blogThumbnail = productImage.imageHtml;
                                        }
                                    });
                                    jQuery("#hiddencontainer .product_row").clone().appendTo(".vmCartModule .vm_cart_products");
                                    jQuery.each(val, function (key, val) {
                                        if (jQuery("#hiddencontainer .product_row ." + key)) $this.find(".vm_cart_products ." + key + ":last").html(val);
                                    });
                                });
                            });
                        }
                        $this.find(".show_cart").html(datas.cart_show);
                        $this.find(".total_products").html(datas.totalProduct);
                        $this.find(".total").html(datas.billTotal);
                    }
                );
            });
        };
    })(jQuery);
</script>