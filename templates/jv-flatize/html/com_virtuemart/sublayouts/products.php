<?php
/**
 * sublayout products
 *
 * @package	VirtueMart
 * @author Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL2, see LICENSE.php
 * @version $Id: cart.php 7682 2014-02-26 17:07:20Z Milbo $
 */

defined('_JEXEC') or die('Restricted access');
$products_per_row = $viewData['products_per_row'];
$currency = $viewData['currency'];
$showRating = $viewData['showRating'];
$verticalseparator = " vertical-separator";

if ($products_per_row == 5)
{ 
	$products_per_row = 4;
}

if ($products_per_row > 6)
{ 
	$products_per_row = 6;
}


foreach ($viewData['products'] as $type => $products ) {

	$rowsHeight = shopFunctionsF::calculateProductRowsHeights($products,$currency,$products_per_row);

	if(!empty($type) and count($products)>0){
		$productTitle = vmText::_('COM_VIRTUEMART_'.strtoupper($type).'_PRODUCT'); ?>
<div class="<?php echo $type ?>-view">
  <h3><?php echo $productTitle ?></h3>
		<?php // Start the Output
    }

	// Calculating Products Per Row
	$cellwidth = ' width'.floor ( 100 / $products_per_row );

	$BrowseTotalProducts = count($products);

	$col = 1;
	$i = 0;
	$row = 1;

	?>
	<div class="shop-content colsItem<?php echo $products_per_row; ?> ">
	<div class="div_list_products   row gridItem">
	<?php
	
	foreach ( $products as $product ) {

		// Show the vertical seperator
		if ($nb == $products_per_row or $nb % $products_per_row == 0) {
			$show_vertical_separator = ' ';
		} else {
			$show_vertical_separator = $verticalseparator;
		}

		// Show Products ?>
		<div class="vmproduct col-md-<?php  echo (12/$products_per_row) . $show_vertical_separator ?> col-sm-6  col-xs-6 ">
			<div class="item item<?php echo  $i%$products_per_row + 1; ?> ">
				<div class="innerItem">
				
					<div class="moduleItemImage">
					    <div class="product-info">
                        <a href="<?php echo JRoute::_($product->link, false); ?>" data-closeClass="bpopup-close" rel="Detailbpopup"><i class="fa fa-search"></i></a> 
						<a title="<?php echo $product->product_name ?>" href="<?php echo JRoute::_($product->link, false); ?>" class="viewproduct"> <span><i class="fa fa-external-link"></i></span> </a>
							<div class="addtocart"> <span><i class="fa fa-shopping-cart"></i></span> <?php echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$product,'rowHeights'=>$rowsHeight[$row])); ?></div>
					    </div>
						<a title="<?php echo $product->product_name ?>" href="<?php echo JRoute::_($product->link, false); ?>">
							<?php
							echo $product->images[0]->displayMediaThumb('class="ProductImage"', false);
							?>
						</a>
                        
                        
					</div>
					
					<div class="content-item-description">
                    
                    <?php //echo $rowsHeight[$row]['price'] ?>
					<div class="price vm3pr-<?php echo $rowsHeight[$row]['price'] ?>"> <?php
						echo shopFunctionsF::renderVmSubLayout('prices',array('product'=>$product,'currency'=>$currency)); ?>
					</div>
					<h3 class="moduleItemTitle"><?php echo JHtml::link (JRoute::_($product->link, false), $product->product_name); ?>	</h3>

                    <?php if($product->categoryItem){?>
                    <div class="related-categories-list">
                        <?php foreach($product->categoryItem as $k=>$product_cat){ echo ($k==count($product->categoryItem)-1) ? $product_cat['category_name'] : $product_cat['category_name'].', ';}?>
                    </div>
                    <?php }?>
                    
                    <div class="vm-product-descr-container-<?php echo $rowsHeight[$row]['product_s_desc'] ?>">
	
							<?php if(!empty($rowsHeight[$row]['product_s_desc'])){
							?>
							<p class="product_s_desc">
								<?php // Product Short Description
								if (!empty($product->product_s_desc)) {
									echo shopFunctionsF::limitStringByWord ($product->product_s_desc, 60, ' ...') ?>
								<?php } ?>
							</p>
					<?php  } ?>
						</div>

					<div class="vm-product-rating-container">
						<?php echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>$showRating, 'product'=>$product));
						if ( VmConfig::get ('display_stock', 1)) { ?>
							<span class="vmicon vm2-<?php echo $product->stock->stock_level ?>" title="<?php echo $product->stock->stock_tip ?>"></span>
						<?php }
						echo shopFunctionsF::renderVmSubLayout('stockhandle',array('product'=>$product));
						?>
					</div>


						



					<?php //echo $rowsHeight[$row]['customs'] ?>
					<div class="vm3pr-<?php echo $rowsHeight[$row]['customfields'] ?>"> 
					</div>
                    
                    
                    </div>


				</div>
			</div>
		</div>

		<?php
		$i ++;
	}
?>

</div>
</div>

<?php  
  
  

      if(!empty($type)and count($products)>0){
        // Do we need a final closing row tag?
        //if ($col != 1) {
      ?>
    <div class="clear"></div>
  </div>
    <?php
    // }
    }
  }
