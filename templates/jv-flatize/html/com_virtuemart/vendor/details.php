<?php
/**
*
* Description
*
* @package	VirtueMart
* @subpackage vendor
* @author Kohl Patrick, Eugen Stranz
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: default.php 2701 2011-02-11 15:16:49Z impleri $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

?>

	<h1 class="titlePage"><?php echo $this->vendor->vendor_store_name;
	if (!empty($this->vendor->images[0])) { ?>
		<div class="vendor-image">
		<?php echo $this->vendor->images[0]->displayMediaThumb('',false); ?>
		</div>
	<?php
	}
?>	</h1>

<div class="row vendor-details-view">
<div class="col-sm-6">
    <div class="panel panel-default vendor-description">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo JText::_('TPL_VENDOR_DESC'); ?></h3>
        </div>
        <div class="panel-body">
            <?php echo $this->vendor->vendor_store_desc; ?>
            
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="panel panel-default  ">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->vendor->vendor_store_name;?></h3>
        </div>
        <div class="panel-body">
            <?php echo shopFunctionsF::renderVendorAddress($this->vendor->virtuemart_vendor_id);?>
            <?php	echo $this->vendor->vendor_legal_info; ?>


            <div class="clearfix">
            <span class="pull-right">
                        <?php echo $this->linktos ?>
            </span>        
            
                    
                        <?php echo $this->linkcontact ?>
            </div>

            
        </div>
    </div>
</div>
</div>    


