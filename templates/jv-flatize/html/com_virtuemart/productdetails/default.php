<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Eugen Stranz, Max Galt
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 8508 2014-10-22 18:57:14Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

/* Let's see if we found the product */
if (empty($this->product)) {
	echo vmText::_('COM_VIRTUEMART_PRODUCT_NOT_FOUND');
	echo '<br /><br />  ' . $this->continue_link_html;
	return;
}

echo shopFunctionsF::renderVmSubLayout('askrecomjs',array('product'=>$this->product));

vmJsApi::jDynUpdate();
vmJsApi::addJScript('updDynamicListeners',"
jQuery(document).ready(function() { // GALT: Start listening for dynamic content update.
	// If template is aware of dynamic update and provided a variable let's
	// set-up the event listeners.
	if (Virtuemart.container)
		Virtuemart.updateDynamicUpdateListeners();

}); ");

if(vRequest::getInt('print',false)){ ?>
<body onLoad="javascript:print();">
<?php } ?>

<div class="productdetails-view productdetails">

<div class="row">
	<div class="col-sm-5">
    
    <script type="text/ecmascript">


(function($){
	$(document).ready(function(){
		function bImage(el){
			el.bxSlider({
				controls: false,
				pagerCustom: '.shop-slider-pager'
				
			});	
		$("a[rel^='prettyPhoto']").prettyPhoto({social_tools : false, deeplinking: false});
		};
		bImage($('.bxslider-shop:visible'));
	});	

})(jQuery);	
</script>

    <div class="vm-product-media-container">
	<?php
    echo $this->loadTemplate('images');
    ?>
	</div>
    
    <?php
		$count_images = count ($this->product->images);
	if ($count_images > 1) {
		echo $this->loadTemplate('images_additional');
	}
	
	?>
    
    
      
      
  
   
    </div>
	<div class="col-sm-7">

			<?php
            // Product Navigation
            if (VmConfig::get('product_navigation', 1)) {
            ?>
                <div class="product-neighbours">
                <?php
                if (!empty($this->product->neighbours ['previous'][0])) {
                $prev_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['previous'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
                echo JHtml::_('link', $prev_link, $this->product->neighbours ['previous'][0]
                    ['product_name'], array('rel'=>'prev', 'class' => 'previous-page pull-left','data-dynamic-update' => '1'));
                }
                if (!empty($this->product->neighbours ['next'][0])) {
                $next_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['next'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
                echo JHtml::_('link', $next_link, $this->product->neighbours ['next'][0] ['product_name'], array('rel'=>'next','class' => 'next-page pull-right','data-dynamic-update' => '1'));
                }
                ?>

                </div>
            <?php } // Product Navigation END
            ?>
        
            <?php // Back To Category Button
            if ($this->product->virtuemart_category_id) {
                $catURL =  JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$this->product->virtuemart_category_id, FALSE);
                $categoryName = $this->product->category_name ;
            } else {
                $catURL =  JRoute::_('index.php?option=com_virtuemart');
                $categoryName = vmText::_('COM_VIRTUEMART_SHOP_HOME') ;
            }
            ?>


            <div class="moreicons">
			<?php
            // Product Edit Link
            echo $this->edit_link;
            // Product Edit Link END
            ?>
        
            <?php
            // PDF - Print - Email Icon
            if (VmConfig::get('show_emailfriend') || VmConfig::get('show_printicon') || VmConfig::get('pdf_icon')) {
            ?>
                <div class="icons">
                <?php
        
                $link = 'index.php?tmpl=component&option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->virtuemart_product_id;
        
                echo $this->linkIcon($link . '&format=pdf', 'COM_VIRTUEMART_PDF', 'pdf_button', 'pdf_icon', false);
                //echo $this->linkIcon($link . '&print=1', 'COM_VIRTUEMART_PRINT', 'printButton', 'show_printicon');
                echo $this->linkIcon($link . '&print=1', 'COM_VIRTUEMART_PRINT', 'printButton', 'show_printicon',false,true,false,'class="printModal"');
                $MailLink = 'index.php?option=com_virtuemart&view=productdetails&task=recommend&virtuemart_product_id=' . $this->product->virtuemart_product_id . '&virtuemart_category_id=' . $this->product->virtuemart_category_id . '&tmpl=component';
                echo $this->linkIcon($MailLink, 'COM_VIRTUEMART_EMAIL', 'emailButton', 'show_emailfriend', false,true,false,'class="recommened-to-friend"');
                ?>
                <div class="clear"></div>
                </div>
            <?php } // PDF - Print - Email Icon END
            ?>
            </div>
                    
            <?php // Product Title   ?>
            <h1><?php echo $this->product->product_name ?></h1>
            <?php // Product Title END   ?>
        
            <?php // afterDisplayTitle Event
            echo $this->product->event->afterDisplayTitle ;
        
            echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>$this->showRating,'product'=>$this->product));
        

			//In case you are not happy using everywhere the same price display fromat, just create your own layout
			//in override /html/fields and use as first parameter the name of your file
			echo shopFunctionsF::renderVmSubLayout('prices',array('product'=>$this->product,'currency'=>$this->currency));
				           
            // Product Short Description
            if (!empty($this->product->product_s_desc)) {
            ?>
                <div class="product-short-description">
                <?php
                /** @todo Test if content plugins modify the product description */
                echo nl2br($this->product->product_s_desc);
                ?>
                </div>
            <?php
            } // Product Short Description END
        
            echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'ontop'));
            ?>
        
            <div class="vm-product-container">
            
        
            <div class="vm-product-details-container">
                <div class="spacer-buy-area">
        

        
                <?php

        
                if (is_array($this->productDisplayShipments)) {
                    foreach ($this->productDisplayShipments as $productDisplayShipment) {
                    echo $productDisplayShipment ;
                    }
                }
                if (is_array($this->productDisplayPayments)) {
                    foreach ($this->productDisplayPayments as $productDisplayPayment) {
                    echo $productDisplayPayment ;
                    }
                }
        

				
				
				// Ask a question about this product
                if (VmConfig::get('ask_question', 0) == 1) {
                    $askquestion_url = JRoute::_('index.php?option=com_virtuemart&view=productdetails&task=askquestion&virtuemart_product_id=' . $this->product->virtuemart_product_id . '&virtuemart_category_id=' . $this->product->virtuemart_category_id . '&tmpl=component', FALSE);
                    ?>

                        <a class="ask-a-question btn btn-grey icon-pencil2" href="<?php echo $askquestion_url ?>" rel="nofollow" title="<?php echo vmText::_('COM_VIRTUEMART_PRODUCT_ENQUIRY_LBL') ?>" ></a>

                <?php
                }

				
				
				
				
                echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$this->product));
        
                echo shopFunctionsF::renderVmSubLayout('stockhandle',array('product'=>$this->product));

?>
				<div class="moreinfo-details">
                <div class="back-to-category">
                	<span><?php echo JText::_('TPL_ITEM_PRODUCT_CATEGORIES'); ?></span><a href="<?php echo $catURL ?>" class="product-details" title="<?php echo $categoryName ?>"><?php echo $categoryName ?></a>
                </div>
<?php
        

                // Manufacturer of the Product
                if (VmConfig::get('show_manufacturers', 1) && !empty($this->product->virtuemart_manufacturer_id)) {
                    echo $this->loadTemplate('manufacturer');
                }

                
                // Product Packaging
				$product_packaging = '';
				if ($this->product->product_box) {
				?>
					<div class="product-box">
                    <span><?php echo JText::_('TPL_VIRTUEMART_PRODUCT_UNITS_IN_BOX'); ?></span>
					<?php
						echo vmText::_('') .$this->product->product_box;
					?>
					</div>
				<?php } // Product Packaging END ?>
                
                
                </div>
        
                </div>
            </div>

        
        
            </div>


<div id="shopaccordion" class="panel-group about-sale-item">

<div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title"><a href="#collapse10" data-parent="#shopaccordion" data-toggle="collapse"><?php echo JText::_('TPL_REVIEWS') ?></a></h4>
    </div>
    <div id="collapse10" class="panel-collapse collapse in">
      <div class="panel-body">
      	<?php echo $this->loadTemplate('reviews'); ?>
      </div>
    </div>
  </div>

<?php if (!empty($this->product->product_desc)) { ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title"><a href="#collapse1" data-parent="#shopaccordion" data-toggle="collapse"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_DESC_TITLE') ?></a></h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse ">
      <div class="panel-body">
      	<?php echo $this->product->product_desc; ?>
      </div>
    </div>
  </div>
<?php } ?> 

    
<?php echo shopFunctionsF::renderVmSubLayout('customfields_more',array('product'=>$this->product,'position'=>'normal')); ?>
      





<?php if ($this->category->haschildren) { ?>  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title"><a href="#collapse4" data-parent="#shopaccordion" data-toggle="collapse"><?php echo JText::_('TPL_CHILD_CATEGORY'); ?></a></h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <div class="panel-body">
      
      <?php echo $this->loadTemplate('showcategory'); ?>
      
      </div>
    </div>
  </div>
<?php } ?>  

<?php  echo shopFunctionsF::renderVmSubLayout('related_categories',array('product'=>$this->product,'position'=>'related_categories','class'=> 'product-related-categories aaaaaaaaaaaaaa')); ?>


 
</div>








        <?php
        
        
            // event onContentBeforeDisplay
            echo $this->product->event->beforeDisplayContent;
        

            echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'onbot'));
        
            

           
            ?>
        
        <?php // onContentAfterDisplay event
        echo $this->product->event->afterDisplayContent; ?>
        
        


            <?php
            echo vmJsApi::writeJS();
            ?>
        </div>
</div>


<?php echo shopFunctionsF::renderVmSubLayout('related_products',array('product'=>$this->product,'position'=>'related_products','class'=> 'product-related-products','customTitle' => true )); ?>

</div>
<script>
	// GALT
	/*
	 * Notice for Template Developers!
	 * Templates must set a Virtuemart.container variable as it takes part in
	 * dynamic content update.
	 * This variable points to a topmost element that holds other content.
	 */
	// If this <script> block goes right after the element itself there is no
	// need in ready() handler, which is much better.
	//jQuery(document).ready(function() {
	Virtuemart.container = jQuery('.productdetails-view');
	Virtuemart.containerSelector = '.productdetails-view';
	//Virtuemart.container = jQuery('.main');
	//Virtuemart.containerSelector = '.main';
	//});

	// Open print and manufacturer link to Modal window
	  <?php if(VmConfig::get('usefancy',1)) : 
	  $manulink = JRoute::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_manufacturer_id=' . $this->product->virtuemart_manufacturer_id[0] . '&tmpl=component', FALSE);
	  ?>
	  jQuery('a.printModal').click(function(e){
		  jQuery.fancybox({
					href: '<?php echo $link.'&print=1'; ?>',
	                type: 'iframe',
	                height: '500'
				  });
                  e.preventDefault();
	  });

	  jQuery('a.manuModal').click(function(e){
		   jQuery.fancybox({
					href: '<?php echo $manulink ?>',
	                type: 'iframe'
				  });
	              e.preventDefault();
	  });
	  
	  <?php else : 
	  $manulink = JRoute::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_manufacturer_id=' . $this->product->virtuemart_manufacturer_id[0] . '&tmpl=component', FALSE);
	  ?>

	  jQuery('a.printModal').click(function(e){
		  jQuery.facebox({
					iframe: '<?php echo $link.'&print=1'; ?>',
					rev: 'iframe|550|550'
				  });
		          e.preventDefault();
	  });

	  jQuery('a.manuModal').click(function(e){
			jQuery.facebox({
					iframe: '<?php echo $manulink; ?>',
					rev: 'iframe|550|550'
					});
			        e.preventDefault();
      });
      
	  <?php endif; ?>	
	  
	    	
</script>


