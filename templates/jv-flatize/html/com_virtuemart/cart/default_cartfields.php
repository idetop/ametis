<?php
// Status Of Delimiter
$closeDelimiter = false;
$openTable = true;
$hiddenFields = '';

if(!empty($this->userFieldsCart['fields'])) {

	// Output: Userfields
	foreach($this->userFieldsCart['fields'] as $field) {
	?>

<fieldset class="col-xs-6 vm-fieldset-<?php echo str_replace('_','-',$field['name']) ?>">

  <div class="panel panel-default  cart <?php echo str_replace('_','-',$field['name']) ?>">
    <div class="panel-heading">
      <h3 class="panel-title cart <?php echo str_replace('_','-',$field['name']) ?>"><?php echo $field['title'] ?></h3>
    </div>
    <div class="panel-body">
    <?php
		if ($field['hidden'] == true) {
			// We collect all hidden fields
			// and output them at the end
			$hiddenFields .= $field['formcode'] . "\n";
		} else { ?>
    <?php echo $field['formcode'] ?> 
    </div>
  </div>

  <?php } ?>



  
</fieldset>


<?php
	}
	// Output: Hidden Fields
	echo $hiddenFields;
}
?>

