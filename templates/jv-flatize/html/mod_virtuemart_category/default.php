<?php // no direct access
defined('_JEXEC') or die('Restricted access');
//JHTML::stylesheet ( 'menucss.css', 'modules/mod_virtuemart_category/css/', false );

if(!function_exists('getMultiCats')){
    function getMultiCats($category, $class_sfx, $active_category_id, $parentCategories){
        $categoryModel = VmModel::getModel('Category');
        $childs = $categoryModel->getChildCategoryList(1, $category->virtuemart_category_id);
        $active_menu = '';
        if (in_array( $category->virtuemart_category_id, $parentCategories)) $active_menu = 'class="active "';
        $caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id);
        $cattext = $category->category_name;
        ?>
        <li <?php echo $active_menu ?>>
            <?php echo JHTML::link($caturl, $cattext); ?>
            <?php if($childs){?>
                <ul class="menu<?php echo $class_sfx; ?>">
                    <?php foreach($childs as $child) getMultiCats($child, $class_sfx, $active_category_id, $parentCategories);?>
                </ul>
            <?php }?>
        </li>
        <?php
    }
}
?>

<ul class="vm-menu-category menu<?php echo $class_sfx ?> menu-sliders" >
    <?php foreach ($categories as $category) getMultiCats($category, $class_sfx, $active_category_id, $parentCategories);?>
</ul>
