<?php
/**
 * @version		$Id: item_comments_form.php 1992 2013-07-04 16:36:38Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<h3><?php echo JText::_('K2_LEAVE_A_COMMENT') ?></h3>

<?php if($this->params->get('commentsFormNotes')): ?>
<p class="itemCommentsFormNotes">
	<?php if($this->params->get('commentsFormNotesText')): ?>
	<?php echo nl2br($this->params->get('commentsFormNotesText')); ?>
	<?php endif; ?>
</p>
<?php endif; ?>

<form action="<?php echo JURI::root(true); ?>/index.php" method="post" id="comment-form" class="form-validate">
	
    
    <div class="row">


	
	<label class="rowlabel col-sm-6"> <span class="spanlable"><?php echo JText::_('TPL_YOUR_NAME'); ?></span>  
	<input class="inputbox" type="text" name="userName" id="userName" value=""  />
    </label>

	<label class="rowlabel col-sm-6"> <span class="spanlable"><?php echo JText::_('TPL_YOUR_EMAIL_ADDRESS'); ?></span>  
	<input  class="inputbox" type="text" name="commentEmail" id="commentEmail" value="" />
    </label>
	
 </div>	

	<label class="rowlabel"> <span class="spanlable"><?php echo JText::_('TPL_YOUR_SITE_URL'); ?></span>  
	<input class="inputbox" type="text" name="commentURL" id="commentURL" value=""  />
    </label>	
	


    
	<label class="rowlabel"> <span class="spanlable"><?php echo JText::_('TPL_YOUR_MESSAGE_HERE'); ?></span>  
	<textarea rows="20" cols="10"     name="commentText" id="commentText"></textarea>
    </label>	
    

    
    <?php if($this->params->get('recaptcha') && ($this->user->guest || $this->params->get('recaptchaForRegistered', 1))): ?>
	<label class="formRecaptcha"> <span class="spanlable"><?php echo JText::_('K2_ENTER_THE_TWO_WORDS_YOU_SEE_BELOW'); ?> </label>
	<div id="recaptcha"></div>
	<?php endif; ?>


    
<button class="btn" name="Submit"  id="submitCommentButton"  type="submit"><?php echo JText::_('K2_SUBMIT_COMMENT'); ?></button>

	<span id="formLog"></span>

	<input type="hidden" name="option" value="com_k2" />
	<input type="hidden" name="view" value="item" />
	<input type="hidden" name="task" value="comment" />
	<input type="hidden" name="itemID" value="<?php echo JRequest::getInt('id'); ?>" />
	<?php echo JHTML::_('form.token'); ?>
</form>
