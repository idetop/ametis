<?php
/**
# mod_jvdemo - JV Demo Module
# @version		1.0.0
# ------------------------------------------------------------------------
# author    Open Source Code Solutions Co
# copyright Copyright (C) 2014 joomlavi.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
# Websites: http://www.joomlavi.com
# Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/
$bgDefault = $jvoption->get('styles.background');
$sDefault = $jvoption->get('styles.themestyle');
defined('_JEXEC') or die('Restricted access');
$bgColor = $jvoption->get('styles.bgcolor');
JHtml::_('behavior.colorpicker');
?>

<script type="text/javascript">
(function($){
	$(document).ready(function(){
		$item1 = $('body');
		$('#demo-list-bg,#demo-list-box').each(function(){
			var $btns = $(this).find('a').click(function(){
				$item1
					.removeClass($btns.filter('.active').removeClass('active').data('value'))
					.addClass($(this).addClass('active').data('value'));
			});
		});
		
		$('#demo-fonts').find('select').each(function(){
			var 
				select = $(this).change(function(){
					$item1.attr('demofont-' + name,select.val());
				}),
				name = select.data('assign')
			;
		});

		//////////////////////////// switcher 
		$switcher = $('#switcher')
		$('.show-switcher-icon').click(function(){
			if($switcher.hasClass('show-switcher')){
				$switcher.removeClass('show-switcher');
			}else{
				$switcher.addClass('show-switcher');
			}
		});	
		
		// change color
		var oCtrlColor = $('.themecolor-color').data('minicolorsSettings');
		!oCtrlColor || (function(ctrl, target){
			ctrl.change = function(c){
				target.css({backgroundColor: c});
			};
		})(oCtrlColor, $item1);
	
	});	

})(jQuery);		

</script>
<ul class="switcher">
<li class="switcher-box selectbox" id="demo-fonts">
        <h5>Font</h5>

<ul>
	<li>
        <p class="font-title">Body:</p>
        <select data-assign="body">
            <option value="f1">Raleway</option>
            <option value="f2">Roboto Slab</option>       
            <option value="f3">Open Sans</option>
            <option value="f4">Oswald</option>
            <option value="f5">Lato</option>
            <option value="f7">Source Sans Pro</option>
            <option value="f8">PT Sans</option>
            <option value="f9">Droid Serif</option>
     
            
            
            
            </select>    
    </li>
    <li>
        <p class="font-title">Menu:</p>
        <select data-assign="menu">
            <option value="f2">Roboto Slab</option>      
            <option value="f1">Raleway</option>
            <option value="f3">Open Sans</option>
            <option value="f4">Oswald</option>
            <option value="f5">Lato</option>
            <option value="f7">Source Sans Pro</option>
            <option value="f8">PT Sans</option>
            <option value="f9">Droid Serif</option>
            </select>    
    </li>
    <li>
        <p class="font-title">Title:</p>
        <select data-assign="header">
            <option value="f2">Roboto Slab</option>     
            <option value="f1">Raleway</option>
            <option value="f3">Open Sans</option>
            <option value="f4">Oswald</option>
            <option value="f5">Lato</option>
            <option value="f7">Source Sans Pro</option>
            <option value="f8">PT Sans</option>
            <option value="f9">Droid Serif</option>
        </select>    
    </li>    
</ul>



       



        



       

        <p class="font-note">* Fonts are used to example. You able to use 600+ google web fonts in the backend.</p>
    </li>
    <li class="switcher-box ">
        <h5>Layout Style</h5>
       
        <ul class="demo-list-box" id="demo-list-box">
			<li class="row">
            <?php foreach ($jStyle as $jk=>$jv){ ?>
            <?php $jChecked = ''; if($sDefault == $jk) $jChecked = 'active '; ?>
            
            
            <div class="col-sm-6">
                 <a  class="btn-sm <?php echo $jChecked; ?> btn  btn-black <?php echo $jk; ?>-style" data-value="<?php echo 'body-'.$jk; ?>" href="javascript:void(0)"><?php echo $jv; ?></a>
			</div>
               
            
            <?php } ?>
            

			</li>               
        </ul>      

    </li>
    <li class="switcher-box">
        <h5>
            <?php // echo implode(', ', $background); ?>
        Texture for Boxed, Framed, Rounded Layout Background</h5>
		<p class="bgcolor">
		<input type="text" class="minicolors themecolor-color" placeholder="<?php echo $bgColor;?>" value="<?php echo $bgColor;?>" />
		</p>
        <ul class="demo-list-bg" id="demo-list-bg">
            <?php foreach ($background as $f=>$text){ ?>
            <?php $jSelected = ''; if($bgDefault == $f) $jSelected = 'active '; ?>
                <li><a class="<?php echo $jSelected; ?> <?php echo modJVDemoHelper::getValue($f); ?>" data-value="<?php echo modJVDemoHelper::getValue($f); ?>" href="javascript:void(0)"></a></li>
            <?php } ?>
        </ul>
    </li>
    
</ul>