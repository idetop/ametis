<?php // no direct access
defined ('_JEXEC') or die('Restricted access');
// add javascript for price and cart, need even for quantity buttons, so we need it almost anywhere
vmJsApi::jPrice();


$col = 1;
$pwidth = ' col-md-' . floor (12 / $products_per_row);

$columnsitem = $products_per_row;
if ( $columnsitem < 1)
	$columnsitem = 1;

?>


	<?php if ($headerText) { ?>
	<div class="vmheader"><?php echo $headerText ?></div>
	<?php
}
	if ($display_style == "div") {
		?>
		<div class="vmgroup<?php echo $params->get ('moduleclass_sfx') ?> colsItem<?php echo $products_per_row; ?> ">
			<div class="div_list_products   row gridItem vmproduct<?php echo $params->get ('moduleclass_sfx'); ?> productdetails">
				<?php $i = 0; foreach ($products as $product) { ?>
				<div class="<?php echo $pwidth;?>  col-sm-6  col-xs-6 ">
					<div class="item item<?php echo  $i%$products_per_row + 1; ?> ">
						<div class="innerItem">
							<div class="moduleItemImage">
								<div class="product-info"> 
									<a title="<?php echo $product->product_name ?>" href="<?php echo $url ?>" class="viewproduct"> <span><i class="fa fa-external-link"></i></span> </a>
									
									<?php if ($show_addtocart) { ?>
									<div class="addtocart"> 
										<span><i class="fa fa-shopping-cart"></i></span> <?php echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$product));?>
										
									</div>
									<?php } ?>
								</div>
								<a title="<?php echo $product->product_name ?>" href="<?php echo $url ?>">
									<?php
									if (!empty($product->images[0])) {
										$image = $product->images[0]->displayMediaThumb ('class="featuredProductImage" border="0"', FALSE);
									} else {
										$image = '';
									}
									echo JHTML::_ ('link', JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id), $image, array('title' => $product->product_name));
									$url = JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' .
										$product->virtuemart_category_id); ?>
								</a>
							</div>
							
							<div class="content-item-description">
							
								
								<?php   
								// $product->prices is not set when show_prices in config is unchecked
								if ($show_price and  isset($product->prices)) {
									echo '<div class="price">'.$currency->createPriceDiv ('salesPrice', '', $product->prices, FALSE, FALSE, 1.0, TRUE);
									if ($product->prices['salesPriceWithDiscount'] > 0) {
										echo $currency->createPriceDiv ('salesPriceWithDiscount', '', $product->prices, FALSE, FALSE, 1.0, TRUE);
									}
									echo '</div>';
								}

								?>
								
								<h3 class="moduleItemTitle"><a href="<?php echo $url ?>"><?php echo $product->product_name ?></a> 	</h3>

								<?php if($product->categoryItem){?>
								<div class="related-categories-list">
									<?php foreach($product->categoryItem as $k=>$product_cat){ echo ($k==count($product->categoryItem)-1) ? $product_cat['category_name'] : $product_cat['category_name'].', ';}?>
								</div>
								<?php }?>							
							
								
							
							</div>
						</div>
					</div>
				</div>
				<?php
					$i++;
				} ?>
			</div>
		</div>


		<?php
	} 
	else {
	
		$last = count ($products) - 1;
		?>

		<div class="div_products_item_n owl-carousel colsItem1">
			<?php 
			foreach ($products as $i =>  $product) :

			?>
            
            	<?php if($i%$columnsitem === 0) echo '<div class="item">'; ?>
				<div class="itemproduct">
					<div class="innerItem">
					
						<div class="moduleItemImage">
							<div class="product-info"> 
								<a title="<?php echo $product->product_name ?>" href="<?php echo $url ?>" class="viewproduct"> <span><i class="fa fa-external-link"></i></span> </a>
								
								<?php if ($show_addtocart) { ?>
								<div class="addtocart"> 
									<span><i class="fa fa-shopping-cart"></i></span> <?php echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$product));?>
									
								</div>
								<?php } ?>
							</div>
							<a title="<?php echo $product->product_name ?>" href="<?php echo $url ?>">
								<?php
								if (!empty($product->images[0])) {
									$image = $product->images[0]->displayMediaThumb ('class="featuredProductImage" border="0"', FALSE);
								} else {
									$image = '';
								}
								echo JHTML::_ ('link', JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id), $image, array('title' => $product->product_name));
								$url = JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' .
									$product->virtuemart_category_id); ?>
							</a>
						</div>
						
						<div class="content-item-description">
						
							
							<?php   
							// $product->prices is not set when show_prices in config is unchecked
							if ($show_price and  isset($product->prices)) {
								echo '<div class="price">'.$currency->createPriceDiv ('salesPrice', '', $product->prices, FALSE, FALSE, 1.0, TRUE);
								if ($product->prices['salesPriceWithDiscount'] > 0) {
									echo $currency->createPriceDiv ('salesPriceWithDiscount', '', $product->prices, FALSE, FALSE, 1.0, TRUE);
								}
								echo '</div>';
							}

							?>
							
							<h3 class="moduleItemTitle"><a href="<?php echo $url ?>"><?php echo $product->product_name ?></a> 	</h3>

							<?php if($product->categoryItem){?>
							<div class="related-categories-list">
								<?php foreach($product->categoryItem as $k=>$product_cat){ echo ($k==count($product->categoryItem)-1) ? $product_cat['category_name'] : $product_cat['category_name'].', ';}?>
							</div>
							<?php }?>							
						
							
						
						</div>
						

					</div>
				</div>
                <?php if(($i + 1)%$columnsitem === 0 || $i === count($products) - 1) echo '</div>';?>
			<?php

			endforeach; ?>
		</div>


		<?php
	}
	if ($footerText) : ?>
		<div class="vmfooter<?php echo $params->get ('moduleclass_sfx') ?>">
			<?php echo $footerText ?>
		</div>
		<?php endif; ?>
