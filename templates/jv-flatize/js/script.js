// JavaScript Document

(function($){
	$(document).ready(function(){
		$('#owl-main-demo').owlCarousel({
			loop:true,
			items:1,
			margin:0,
			nav:true,
		})
		
		/////////////////////////////  Popup product detail 
		var PDetail = $('#PopupPDetail');
		$('body').delegate('[rel=Detailbpopup]','click',function(){
			var link = $(this).attr('href');
			if(link.indexOf('?') !== false) link += '?';
			PDetail.find('iframe').attr('src',  link + '&tmpl=component' );
			PDetail.bPopup({closeClass:'bpopup-close'});
			return false;
		});		


		/////////////////////////////  add class

		$('.moduleParallax1').each(function(){$(this).parent().parent().parent().addClass('sectionParallax1');});	
		$('.moduleParallax2').each(function(){$(this).parent().parent().parent().addClass('sectionParallax2');});
		$('h1.titlePage .continue_link').each(function(){$(this).addClass('btn btn-primary btn-sm');});	
		$('.checkout-button-top #checkoutFormSubmit, .page-checkout-done .vm-button-correct').each(function(){$(this).addClass('btn btn-primary ');});	
		
		$('#ui-tabs table.sectiontableheader').each(function(){$(this).addClass('table table-striped table-hover table-bordered');});
		
		
		
		

		
			/* Categories K2*/
			$('.k2CategoriesListBlock div.categories > ul').addClass('menu-sliders');
			$('.k2CategoriesListBlock div.categories .activeCategory').addClass('active');
			$('.k2CategoriesListBlock div.categories ul[class*="level"]').each(function(){$(this).parent().addClass('parent');});	
			$('.vm-menu-category  ul').each(function(){$(this).parent().addClass('parent');});	
			$('ul.menu-sliders li.active').parent().parent().addClass('parent-showsub');

			
			
			
		
		




		//////////////////////////////	


		$('ul.menu-sliders li.parent').each(function(){
			 var 
				p = $(this),
				btn = $('<span>',{'class':'showsubmenu icon-angle-down', text : '' }).click(function(){
					if(p.hasClass('parent-showsub')){
						menu.slideUp(300,function(){
							p.removeClass('parent-showsub');
						});        										
					}else{
						menu.slideDown(300,function(){
							p.addClass('parent-showsub');
						});        										
					};	
				}),
				menu = p.children('ul')
			 ;
			 p.prepend(btn)  // append : trong - duoi, prepend : trong - tren, after : duoi, before : tren
		});	
		
		///////////





		
		
	 	$(".selectbox select, .hikashop_products_pagination select, .product-field-display select, .orderby-displaynumber  select").selectify();	
		
		
		///////////////////////////// prettyPhoto
		
		$("a[rel^='prettyPhoto']").prettyPhoto();





			
			
/////////////////////////////  


		/* sidebar mobile */
		$('.sidebarToggle').click(function(){
			if($('body').hasClass('showSidebar')){
				$('body').removeClass('showSidebar');
			}else{
				$('body').addClass('showSidebar');
				//$('.flexMenuToggle:first').click();
			return false;
			}
		});	
		
		
		/* Search */
		var searchBar = $('#searchtop');
		$('.btnsearchtop').click(function(){
			searchBar.slideToggle(300);
			$('.flexMenuToggle:first').click();
			return false;
		});			 
		$('#search-beack').click(function(){
			searchBar.slideUp(300);
			return false;
		});



		/* Login */
		$('.iconlogin').click(function(){
			if($('.loginform').hasClass('show')){
				$('.loginform').removeClass('show');
			}else{
				$('.loginform').addClass('show');
				$('.flexMenuToggle:first').click();
			return false;
			}
		});	
        
       /* Version 4 */
       		$('.btnsearchtop2').click(function(){
    			searchBar.slideToggle(300);
    			return false; 
		   });	  
          
            /* Login */
    		$('.position-header-right .iconlogin2').click(function(){
    			if($('.loginform').hasClass('show')){
    				$('.loginform').removeClass('show');
    			}else{
    				$('.loginform').addClass('show');
    			return false;
    			}
    		});		

		
		/* Show sub categori Mobile*/

		$('.btnShowSubcategoriesMobile').click(function(){
			$('.subcategoriesMobile').slideToggle(300);
		});	

		/* Panel Mobile  */
		$('.btnPanel').click(function(){
			if($('#block-panel').hasClass('show')){
				$('#block-panel').removeClass('show');
				$('#block-panel .container').slideUp(250);
			}else{
				$('#block-panel').addClass('show');
				$('#block-panel .container').slideDown(250);
			}
		});			
								
		
				
		////////////////////////// Move
		
		if ($('#block-breadcrumb .innercontainer').length) {
			$('h1.titlePage').prependTo("#block-breadcrumb .innercontainer");
		};
		
		$('.productdetails-view .spacer-buy-area a.ask-a-question').prependTo(".productdetails-view .spacer-buy-area .addtocart-bar ");		



		

		
			
		

		///////////////////////////// Parallax
		
		$(function(){		
            $('.sectionParallax1, .sectionParallax2').parallax();
		});	





				

			
	});	
})(jQuery);	

(function($){
	$(document).ready(function(){

		"use strict";

		var Core = window.TMPInit =  {

			initialized: false,

			initialize: function() {

				if (this.initialized) return;
				this.initialized = true;

				this.build();

			},

			build: function() {
				
				$('body').bind('touchstart', function() {});
				
				// Scroll to Top Button.
				//$.scrollToTop();
				
		
				// Owl Carousel
				this.owlCarousel();
				
				// Owl Carousel 2
				this.owlSecondCarousel();
				
				// Owl Carousel 3
				this.owlThirdCarousel();
				
				// Products Owl Carousel
				this.owlProducts();
				

				//Related Products Owl Carousel
				this.owlRelatedProducts();				
				

				// Products List Owl Carousel
				this.owlProductsList();
				
				
				// Text Owl Carousel
				this.owlText();

				// Text Owl Partner
				this.owlPartner();				
				
				
				
				
				// Owl Featured Carousel
				this.owlFeaCarousel();
				
				//Owl Blog Slide
				this.owlBlogSlide();
				this.detailAdd();
				
				// Tooltips
				$("a[data-toggle=tooltip]").tooltip();
				
				// bsSlider
	/*			$('.bxslider-shop').bxSlider({
					controls: false,
					pagerCustom: '.shop-slider-pager'
				});*/

				

			},

			owlCarousel: function(options) {
				
				//new listing
				var owl = $(".owl_slideshow1");
				
				owl.owlCarousel({
					navigation : true,
					pagination: false,
					singleItem : true,
					slideSpeed: 200,
					autoPlay : 5000,
					transitionStyle : "fadeUp",
					navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
				  });
		
				  //Select Transtion Type
				  $("#transitionType").change(function(){
					var newValue = $(this).val();
		
					//TransitionTypes is owlCarousel inner method.
					owl.data("owlCarousel").transitionTypes(newValue);
		
					//After change type go to next slide
					owl.trigger("owl.next");
				  });
				

			},

			owlSecondCarousel: function(options) {
				
				//new listing
				var owl = $(".owl_slideshow2");
				
				owl.owlCarousel({
					navigation : true,
					pagination: false,
					singleItem : true,
					autoPlay : true,
					transitionStyle : "backSlide",
					navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
				  });
		
				  //Select Transtion Type
				  $("#transitionType").change(function(){
					var newValue = $(this).val();
		
					//TransitionTypes is owlCarousel inner method.
					owl.data("owlCarousel").transitionTypes(newValue);
		
					//After change type go to next slide
					owl.trigger("owl.next");
				  });
				

			},

			owlThirdCarousel: function(options) {
				
				//new listing
				var owl = $(".owl_slideshow3");
				
				owl.owlCarousel({
					navigation : true,
					pagination: false,
					singleItem : true,
					autoPlay : true,
					transitionStyle : "goDown",
					navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
				  });
		
				  //Select Transtion Type
				  $("#transitionType").change(function(){
					var newValue = $(this).val();
		
					//TransitionTypes is owlCarousel inner method.
					owl.data("owlCarousel").transitionTypes(newValue);
		
					//After change type go to next slide
					owl.trigger("owl.next");
				  });
				

			},

			owlFeaCarousel: function(options) {
				
				//owl-featured-slide
				var owl = $("#owl-featured-slide");
				
				owl.owlCarousel({
					navigation : true,
					pagination: false,
					singleItem : true,
					autoPlay : true,
					transitionStyle : "fade",
					navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
				  });
		
				  //Select Transtion Type
				  $("#transitionType").change(function(){
					var newValue = $(this).val();
		
					//TransitionTypes is owlCarousel inner method.
					owl.data("owlCarousel").transitionTypes(newValue);
		
					//After change type go to next slide
					owl.trigger("owl.next");
				  });
				

			},

			owlBlogSlide: function(options) {
				
				//owl-featured-slide
				var owl = $("#owl-blog-slide");
				
				owl.owlCarousel({
					navigation : true,
					pagination: false,
					singleItem : true,
					autoPlay : true,
					transitionStyle : "fade",
					navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
				  });
		
				  //Select Transtion Type
				  $("#transitionType").change(function(){
					var newValue = $(this).val();
		
					//TransitionTypes is owlCarousel inner method.
					owl.data("owlCarousel").transitionTypes(newValue);
		
					//After change type go to next slide
					owl.trigger("owl.next");
				  });
				

			},
			

			owlRelatedProducts: function(options) {
				
				var owl = $(".module_related_products_list_slider");
				owl.owlCarousel({
					navigation : false,
					pagination: false,
					autoPlay : true,
					items : 4,
					itemsCustom : false,
					itemsDesktop : [1199,4],
					itemsDesktopSmall : [991,3],
					itemsTablet: [768,3],
					itemsTabletSmall: [640,2],
					itemsMobile : [480,1],
					singleItem : false,
					itemsScaleUp : false,
					navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
					
				});
				

				

			},


			owlProducts: function(options) {
				
				var owl = $(".module_products_list_slider");
				owl.owlCarousel({
					navigation : true,
					pagination: false,
					//autoPlay : true,
					items : 4,
					itemsCustom : false,
					itemsDesktop : [1199,4],
					itemsDesktopSmall : [991,3],
					itemsTablet: [768,3],
					itemsTabletSmall: [640,2],
					itemsMobile : [480,1],
					singleItem : false,
					itemsScaleUp : false,
					navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
					
				});
				
				// Custom Navigation Events
				$("#owl-product-slide .next").click(function(){
					owl.trigger('owl.next');
				})
				
				$("#owl-product-slide .prev").click(function(){
					owl.trigger('owl.prev');
				})
				

			},

			owlProductsList: function(options) {
				
				var owl = $(".div_products_item_n");
				owl.owlCarousel({
					navigation : true,
					pagination: false,
					autoPlay : true,
					singleItem : true,				
					navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
					
				});
			},		
			
			owlText: function(options) {
				
				//new listing
				var owl = $("#owl-text-slide");
				
				owl.owlCarousel({
					navigation : false,
					pagination : false,
					singleItem : true,
					// autoPlay : true,
					transitionStyle : "fade"
				  });
		
				  //Select Transtion Type
				  $("#transitionType").change(function(){
					var newValue = $(this).val();
		
					//TransitionTypes is owlCarousel inner method.
					owl.data("owlCarousel").transitionTypes(newValue);
		
					//After change type go to next slide
					owl.trigger("owl.next");
				  });
				

			},


			owlPartner: function(options) {
				
				//new listing
				var owl = $("#owl-logo-partner");
				
				owl.owlCarousel({
					navigation : true,
					pagination: false,
					autoPlay : true,
					items : 5,
					itemsCustom : false,
					itemsDesktop : [1199,5],
					itemsDesktopSmall : [991,4],
					itemsTablet: [768,3],
					itemsTabletSmall: [640,2],
					itemsMobile : [480,1],
					singleItem : false,
					itemsScaleUp : false,
					navigationText :false				
				
				
				});
		

				

			},
			detailAdd: function(el){
				//.productdetails-view
				el = $(el || '.productdetails-view' );
				el.find('input.addtocart-button').each(function(){$(this).addClass('btn btn-primary');});	
				el.find('.spacer-buy-area a.ask-a-question').prependTo(el.find(".spacer-buy-area .addtocart-bar "));		
			}

			
			
			
		};
	Core.initialize();
	});	
})(jQuery);	




