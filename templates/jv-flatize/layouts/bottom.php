
<?php if( $this['block']->count('fullbottom') ):?>
<!--FullBottom-->
	<section id="block-fullbottom">
         	<jdoc:include type="position" name="fullbottom"  />
    </section>
<!--End FullBottom-->
<?php endif;?>

<?php if( $this['block']->count('top') ):?>
<!--Top-->
	<section id="block-top">
    	<div class="container">
    		<jdoc:include type="block" name="top"  />
        </div>
    </section>
<!--End Top-->    
<?php endif;?>

<?php if( $this['block']->count('user-1') ):?>
<!--user-1-->
	<section id="block-user-1" class="titleCenter">
    	<div class="container">
         	<jdoc:include type="position" name="user-1"  />
        </div>
    </section>
<!--End user-1-->    
<?php endif;?>

<?php if( $this['block']->count('user-2') ):?>
<!--user-2-->
	<section id="block-user-2" class="titleCenter">
    	<div class="container">
         	<jdoc:include type="position" name="user-2"  />
        </div>
    </section>
<!--End user-2-->
<?php endif;?>

<?php if( $this['block']->count('user-3') ):?>
<!--user-3-->
	<section id="block-user-3" class="titleCenter">
    	<div class="container">
         	<jdoc:include type="position" name="user-3"  />
        </div>
    </section>
<!--End user-3-->
<?php endif;?>

<?php if( $this['block']->count('user-4') ):?>
<!--user-4-->
	<section id="block-user-4" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-4"  />
     </div>       
    </section>
<!--End user-4-->
<?php endif;?>

<?php if( $this['block']->count('user-5') ):?>
<!--user-5-->
	<section id="block-user-5" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-5"  />
              </div>
    </section>
<!--End user-5-->
<?php endif;?>

<?php if( $this['block']->count('topb') ):?>
<!--Topb-->
	<section id="block-topb">
    	<div class="container">
    		<jdoc:include type="block" name="topb" />
        </div>
    </section>
<!--End Topb-->
<?php endif;?>

<?php if( $this['block']->count('user-6') ):?>
<!--user-6-->
	<section id="block-user-6" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-6"  />
              </div>
    </section>
<!--End user-6-->
<?php endif;?>

<?php if( $this['block']->count('user-7') ):?>
<!--user-7-->
	<section id="block-user-7" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-7"  />
              </div>
    </section>
<!--End user-7-->
<?php endif;?>

<?php if( $this['block']->count('user-8') ):?>
<!--user-8-->
	<section id="block-user-8" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-8"  />
              </div>
    </section>
<!--End user-8-->
<?php endif;?>




<!--Block Footer-->

<footer id="block-footer" >
    <div class="container">
    

        <?php if( $this['block']->count('bottom') ):?>
		<!--Bottom-->
            <div id="block-bottom" >
                    <jdoc:include type="block" name="bottom"  />
            </div>
        <!--End Bottom-->
		<?php endif;?>

        <?php if( $this['block']->count('bottomb') ):?>
        <!--Bottomb-->
            <div id="block-bottomb">
                    <jdoc:include type="block" name="bottomb" />
            </div>
        <!--End Full Bottomb-->
		<?php endif;?>



        <jdoc:include type="position" name="footer" style="raw"/>
    </div>
</footer>

<!--Block Footer-->

