<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>

	<?php
	echo $this['template']->render('head');	

    include($this['path']->findPath('style.config.php'));
    ?>    
<?php 
$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$ctrl     = $app->input->getCmd('ctrl', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
?>



    
</head>
<body class="<?php echo $this['option']->get('template.body.class'); ?>

<?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '')
	. (isset($customdisplay) ? $customdisplay : '');
?>

<?php 
    if( $this['position']->count('slideshow-full') ) echo ' isSlideshow';
	if ( ( $this['block']->count('breadcrumb') ) && (( $option == 'com_virtuemart') && ( $view == 'productdetails')) )  echo ' isBreadcrumb';
    if ( ($option == 'com_hikashop' && $ctrl == 'product') &&   $this['block']->count('breadcrumb')  )  echo ' isBreadcrumb';
	
	if (  $option == 'com_virtuemart') { 
		if (!in_array($view,array('category','virtuemart','manufacturer'))) {
			echo ' shopFullContent';   
		}
	} 

?>
">

	<div id="wrapper">
        <div id="mainsite">
            <span class="flexMenuToggle" ></span> 
            <?php  echo $this['template']->render('top'); ?>
            <section id="block-main">
                <div class="container">
                	<div class="row">
                        <?php echo $this['template']->render('content'); ?>
                        <?php 
						if (  $option == 'com_virtuemart' ) {
							if (($view == 'virtuemart' ) || ($view == 'category' ) || ($view == 'manufacturer' ) ) {
							 echo $this['template']->render('sidebar-a');             	            
							 echo $this['template']->render('sidebar-b'); 
							}
                        } else {
							 echo $this['template']->render('sidebar-a');             	            
							 echo $this['template']->render('sidebar-b');
						}
						?>
                     </div>   
                </div>
            </section>
            <?php echo $this['template']->render('bottom'); ?>
            
            

            
           
        </div> 
	</div>
    
<?php if( $this['position']->count('demo') ):?>    

<div id="switcher" class="hidden-xs">
		<span class="show-switcher-icon"><i class="icon-pencil7"></i></span>
<div class="inner-switcher">

        <jdoc:include type="position" name="color" style="none" />
		<jdoc:include type="position" name="demo" style="none" />
</div>
</div>

 
<?php endif;?>  

<?php echo $this['position']->render('analytic.none'); ?>  
<div id="PopupPDetail" >
<div class="container"> <span class="bpopup-close"></span> 
<iframe  frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
</div>
</div>




    




    
</body>
</html>