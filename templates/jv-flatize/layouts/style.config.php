<?php
/* Add style*/


if( $this['position']->count('demo') ) {
echo $this['asset']->addStyle($this['path']->url('theme::css/demo-param.css'));	
}


$this['asset']->addScript($this['path']->url('theme::js/script.js'));

//$this['asset']->addScript($this['path']->url('theme::js/theme.plugins.js'));

$this['asset']->addScript($this['path']->url('theme::js/owl.carousel.js')); 
$this['asset']->addScript($this['path']->url('theme::js/modernizr.custom.js'));

$this['asset']->addScript($this['path']->url('theme::js/jquery-selectify.js'));
$this['asset']->addScript($this['path']->url('theme::js/jquery.bxslider.min.js'));
$this['asset']->addScript($this['path']->url('theme::js/jquery.prettyPhoto.js'));

$this['asset']->addScript($this['path']->url('theme::js/parallax-plugin.js'));
$this['asset']->addScript($this['path']->url('theme::js/jquery.bpopup.min.js'));



?>


