<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
<?php
	echo $this['template']->render('head');	

    include($this['path']->findPath('style.config.php'));
    ?>

<?php 
$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$ctrl     = $app->input->getCmd('ctrl', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
?>
    
</head>
<body id="layoutMobile" class="<?php echo $this['option']->get('template.body.class'); ?>
<?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '')
	. (isset($customdisplay) ? $customdisplay : '');
?>

<?php 
    if( $this['position']->count('slideshow-full') ) echo ' isSlideshow';
    if ( ( $option != 'com_hikashop') && ( $this['block']->count('breadcrumb') ) )  echo ' isBreadcrumb';
    if ( ( $option == 'com_hikashop' && $ctrl == 'product') && ( $this['block']->count('breadcrumb') ) )  echo ' isBreadcrumb';

?>

">
<div id="wrapper">

  
  <div id="mainsite"> <span class="flexMenuToggle" ></span>
  
  

<?php if( $this['position']->count('searchtop') ):?>
<!--Search-->
	<div id="searchtop">
    <div class="container">
		<span id="search-beack" >x</span>
        <jdoc:include type="position" name="searchtop"  style="none"  />
     	
     </div>
	</div>
<!-- End Search-->
<?php endif;?>

<?php if( $this['block']->count('panel') ):?>
<!--Block top-->
	<section id="block-panel" class="top-bar">
    	<div class="container">
    		<jdoc:include type="block" name="panel" />
        </div>
        <span class="btnPanel "><i class="fa fa-chevron-down"></i></span> 
    </section>
<!-- End Block top-->
<?php endif;?>
  
    <section  id="block-header">
      <div class="container"> 
      	<a class="flexMenuToggle" href="JavaScript:void(0);" ><span></span><span></span><span></span></a>
        <jdoc:include type="position" name="logo" />
         <jdoc:include type="position" name="header-right" style="notitle" />

      </div>
    </section>
    <!--Block Breadcrumb-->
<section id="block-breadcrumb">
<jdoc:include type="position" name="login" style="none" />
   
</section>
<!-- End Block Breadcrumb-->
    <section  id="block-main">
      <div class="container">
        <jdoc:include type="message" />
        <jdoc:include type="component" />
        <jdoc:include type="position" name="content-top"  />
        <jdoc:include type="position" name="content-bottom"  />
      </div>
    </section>

<?php if( $this['block']->count('user-1') ):?>
<!--user-1-->
	<section id="block-user-1" class="titleCenter">
    	<div class="container">
         	<jdoc:include type="position" name="user-1"  />
        </div>
    </section>
<!--End user-1-->    
<?php endif;?>

<?php if( $this['block']->count('user-2') ):?>
<!--user-2-->
	<section id="block-user-2" class="titleCenter">
    	<div class="container">
         	<jdoc:include type="position" name="user-2"  />
        </div>
    </section>
<!--End user-2-->
<?php endif;?>


<?php if( $this['block']->count('user-3') ):?>
<!--user-3-->
	<section id="block-user-3" class="titleCenter">
    	<div class="container">
         	<jdoc:include type="position" name="user-3"  />
        </div>
    </section>
<!--End user-3-->
<?php endif;?>


<?php if( $this['block']->count('user-4') ):?>
<!--user-4-->
	<section id="block-user-4" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-4"  />
     </div>       
    </section>
<!--End user-4-->
<?php endif;?>

<?php if( $this['block']->count('user-5') ):?>
<!--user-5-->
	<section id="block-user-5" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-5"  />
              </div>
    </section>
<!--End user-5-->
<?php endif;?>

<?php if( $this['block']->count('topb') ):?>
<!--Topb-->
	<section id="block-topb">
    	<div class="container">
    		<jdoc:include type="block" name="topb" />
        </div>
    </section>
<!--End Topb-->
<?php endif;?>

<?php if( $this['block']->count('user-6') ):?>
<!--user-6-->
	<section id="block-user-6" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-6"  />
              </div>
    </section>
<!--End user-6-->
<?php endif;?>

<?php if( $this['block']->count('user-7') ):?>
<!--user-7-->
	<section id="block-user-7" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-7"  />
              </div>
    </section>
<!--End user-7-->
<?php endif;?>

<?php if( $this['block']->count('user-8') ):?>
<!--user-8-->
	<section id="block-user-8" class="titleCenter">
    <div class="container">
         	<jdoc:include type="position" name="user-8"  />
              </div>
    </section>
<!--End user-8-->
<?php endif;?>

    
    <footer id="block-footer">
      <div class="container">
        <jdoc:include type="position" name="footer" style="none"  />
      </div>
    </footer>
    
  </div>

  <div id="block-mainnav-mobile">
    <jdoc:include type="position" name="menu" style="raw" />
  </div>
  
  
<?php if( $this['position']->count('left or left-1 or left-2 or right or right-1 or right-2') ):?>
<!--Sidebar-->
	<section id="block-sidebar" >
    <span class="sidebarToggle" ><i class="icon-settings5"></i></span>
    <div class="inner-sidebar">
            <jdoc:include type="position" name="left-1"  />             
        	<jdoc:include type="position" name="left"  />

            <jdoc:include type="position" name="left-2"  />
            <jdoc:include type="position" name="right-1"  />
            <jdoc:include type="position" name="right"  />

            <jdoc:include type="position" name="right-2"  />
            </div>
    </section>
<!--End Sidebar-->    
<?php endif;?>
  
  
</div>
</body>
</html>