<?php 
$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$ctrl     = $app->input->getCmd('ctrl', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
?>



<?php if( $this['position']->count('searchtop') ):?>
<!--Search-->
	<div id="searchtop">
    <div class="container">
		<span id="search-beack" >x</span>
        <jdoc:include type="position" name="searchtop"  style="none"  />
     	
     </div>
	</div>
<!-- End Search-->
<?php endif;?>



<?php if( $this['block']->count('panel') ):?>
<!--Block top-->
	<section id="block-panel" class="top-bar">
    	<div class="container">
    		<jdoc:include type="block" name="panel" />
        </div>
        <span class="btnPanel "><i class="fa fa-chevron-down"></i></span> 
    </section>
<!-- End Block top-->
<?php endif;?>


<!--Block Header -->
<header id="block-header">
    	<div class="container">
                <jdoc:include type="position" name="logo" />
                <jdoc:include type="position" name="header-right" style="notitle" />
				<?php if( $this['position']->count('menu') ):?>
                    <section id="block-mainnav">
                            <jdoc:include type="position" name="menu" style="none" />
                    </section >
                <?php endif;?>
                <a class="flexMenuToggle" href="JavaScript:void(0);" ><span></span><span></span><span></span></a>                
        </div>
</header>
<!-- End Block Header-->

<!--Block Breadcrumb-->
<section id="block-breadcrumb">
<jdoc:include type="position" name="login" style="none" />
<?php  

if( !$this['position']->count('slideshow-full')){ ?>
    <div class="container"> 
        <div class="innercontainer">
    <?php             
        if ( ($option == 'com_hikashop') || ( $option == 'com_virtuemart')) {
            if ( ($ctrl == 'product') || ( $view == 'productdetails')) { ?>
                    <jdoc:include type="position" name="breadcrumb" style="none" />
    <?php            
            }        
        }
        else { ?>
                    <jdoc:include type="position" name="breadcrumb" style="none" />
    <?php 
        } ?>



        
        </div>
    </div>   
<?php            
}
?>
    
</section>
<!-- End Block Breadcrumb-->


<?php if( $this['position']->count('slideshow-full') ):?>
<!--Block Slide-->
	<section id="block-slide-full" >
		    <jdoc:include type="position" name="slideshow-full"  style="none"  />
	</section>
<!-- End Block Slide-->
<?php endif;?>


<?php if( $this['position']->count('slideshow') ):?>
<!--Block Slideshow-->
	<section id="block-slide">
    		<div class="container"> 
		    <jdoc:include type="position" name="slideshow"  style="none"  />
            </div>
	</section>
<!-- End Block Slideshow-->
<?php endif;?>



<?php if( $this['block']->count('fulltop') ):?>
<!--Block fulltop-->
	<section id="block-fulltop">
         	<jdoc:include type="position" name="fulltop"  />
    </section>
<!-- End Block fulltop-->
<?php endif;?>

