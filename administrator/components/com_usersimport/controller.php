<?php
/**
 * @file       controller.php
 * @brief      Users Import document administration provide a cron task that can automatically fill the docman DB with new files descriptions.
 *
 * @version    1.0.0
 * @author     Edwin CHERONT     (e.cheront@jms2win.com)
 *             Edwin2Win sprlu   (www.jms2win.com)
 * @copyright  Joomla Multi Sites
 *             Single Joomla! 1.5.x installation using multiple configuration (One for each 'slave' sites).
 *             (C) 2012 Edwin2Win sprlu - all right reserved.
 * @license    This program is free software; you can redistribute it and/or
 *             modify it under the terms of the GNU General Public License
 *             as published by the Free Software Foundation; either version 2
 *             of the License, or (at your option) any later version.
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *             A full text version of the GNU GPL version 2 can be found in the LICENSE.php file.
 * @par History:
 * - V1.0.0 27-MAR-2012: File creation
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');



// ===========================================================
//             UsersImportController class
// ===========================================================
/**
 * @brief Multi Sites Article Sharing controler.
 */
class UsersImportController extends JController
{
   //------------ addSubmenu ---------------
   /**
    * Generic function that display the submenu
    */
	function addSubmenu($vName)
	{
      // If Joomla 1.5
      if ( version_compare( JVERSION, '1.5') <= 0) {
         // Don't do anything
         return;
      }
      
      // If Joomla 1.6, build the submenu based on the menu definition present in the DB

	   // Retreive all the submenu
   	$option     = JRequest::getCmd('option');
	   $db =& JFactory::getDBO();
      $query = "SELECT c.title, c.link, c.alias FROM #__menu as p"
             . " LEFT JOIN #__menu as c ON c.parent_id=p.id"
             . " WHERE p.level = 1 AND p.type='component' AND p.title='$option'"
             . " ORDER BY c.id"
             ;
      $db->setQuery( $query );
      $rows = $db->loadObjectList();
      if ( empty( $rows)) {
         return;
      }

		// Load the "system" menu
		$lang	= JFactory::getLanguage();
			$lang->load($option.'.sys', JPATH_ADMINISTRATOR, null, false, false)
		||	$lang->load($option.'.sys', JPATH_ADMINISTRATOR.'/components/'.$option, null, false, false)
		||	$lang->load($option.'.sys', JPATH_ADMINISTRATOR, $lang->getDefault(), false, false)
		||	$lang->load($option.'.sys', JPATH_ADMINISTRATOR.'/components/'.$option, $lang->getDefault(), false, false);
      
      foreach ($rows as $row) {
         // Extract the task value present in the link
         $pos = strpos( $row->link, '?');
         if ( $pos === false) {
            $param_url = $row->link;
         }
         else {
            $param_url = substr( $row->link, $pos+1);
         }
         $params_array = explode( '&', $param_url);
         foreach( $params_array as $param) {
            $keyvalues = explode( '=', $param);
            if ( $keyvalues[0] == 'task') {
               $menuTask = $keyvalues[1];
            }
         }
         
         // If it was not possible to get the task parameter, use the "alias" as name
         if ( empty( $menuTask)) {
            $menuTask = $row->alias;
         }
         
   		JSubMenuHelper::addEntry(
   			JText::_( strtoupper( $row->title)),
   			$row->link,
   			$vName == $menuTask
   		);
      }
	}

	// =====================================
	//             refresh
	// =====================================


   //------------ doimport ---------------
   /**
    * @brief Perform the import of a file.
    */
	function doimport()
	{
		JToolBarHelper::title( JText::_( 'Refresh the docman document definitions'), 'config.png' );
		$model	=& $this->getModel( 'import' );
		$view    =& $this->getView( 'import');
		$view->setModel( $model, true );
		$view->doImport();
		$this->addSubmenu(JRequest::getWord('task', 'about'));
	}
 
   //------------ refresh ---------------
   /**
    * @brief Refresh the documents definition to add the new files.
    */
	function import()
	{
		JToolBarHelper::title( JText::_( 'Import Users into Joomla'), 'config.png' );
		$model	=& $this->getModel( 'import' );
		$view    =& $this->getView( 'import');
		$view->setModel( $model, true );
		$view->Import();
		$this->addSubmenu(JRequest::getWord('task', 'about'));
	}



	// =====================================
	//             LOG FILE
	// =====================================

   // -------------- removelog ------------------------------
   function removelog() {
		$model	=& $this->getModel( 'import' );
		$msg = $model->removelog();
		echo $msg;
		$this->addSubmenu(JRequest::getWord('task', 'about'));
   }
   
   // -------------- deletelog ------------------------------
   function deletelog() {
		JToolBarHelper::title( JText::_( 'Users Import') .' - '.JText::_( 'Delete Logfile'), 'config.png' );
   ?>   
      <form action="index.php?option=com_usersimport&task=removelog" method="POST">
         <table class="adminform">
          <tr class="row0">
            <td class="labelcell"><?php echo JText::_( 'Are you sure you want to delete the Logfile'); ?> ?</td>
          </tr>
          <tr>
            <td><input type="submit" value="Delete"/></td>
          </tr>
        </table>
     </form>
   <?php   
		$this->addSubmenu(JRequest::getWord('task', 'about'));
   }
   
   
   // -------------- showlog ------------------------------
   function showlog() {
		JToolBarHelper::title( JText::_( 'Users Import') .' - '.JText::_( 'Show Logfile'), 'config.png' );
		$model	=& $this->getModel( 'import' );
      $myFilename = $model->getLogFileName();
      if ( !file_exists( $myFilename)){
         echo "No log file";
      }
      else {
         $content = str_replace( "\n", "<br/>", htmlentities( @implode("", (@file("$myFilename")))));
         echo $content;
      }
		$this->addSubmenu(JRequest::getWord('task', 'about'));
   }



	// =====================================
	//             USER MANUAL
	// =====================================
 
   //------------ usersManual ---------------
   /**
    * @brief Redirect to the online User Manual.
    */
	function usersManual()
	{
   	$option = JRequest::getCmd('option');
		$mainframe	= &JFactory::getApplication();
      $version = $this->_getVersion();
      $url = 'http://www.jms2win.com/index.php?option=com_docman&task=findkey&keyref='.$option.'.usersmanual&version='.$version;
      $mainframe->redirect( $url);
		$this->addSubmenu(JRequest::getWord('task', 'about'));
	}


	// =====================================
	//             ABOUT
	// =====================================

   //------------ _getVersion ---------------
   /**
    * @brief Retreive the version number of this component.
    */
	function _getVersion()
	{
	   jimport( 'joomla.application.helper');
	   $version = "unknown";
	   $filename = dirname(__FILE__) .DS. MULTISITES_MANIFEST_FILENAME;
		if ($data = JApplicationHelper::parseXMLInstallFile($filename)) {
		   // If the version is present
		   if (isset($data['version']) && !empty($data['version'])) {
		      $version = $data['version'];
		   }
		}
		return $version;
	}

   //------------ about ---------------
	function about()
	{
		JToolBarHelper::title( JText::_( 'Users Import') .' - '.JText::_( 'About'), 'config.png' );
?>
<h3>Users Import</h3>
<p>Version <?php echo $this->_getVersion(); ?><br/>
<p>Allows refreshing the documents file descriptions based on the current files.</p>
<h3>Copyright</h3>
<p>Copyright 2012 Edwin2Win sprlu<br/>
Rue des robiniers, 107<br/>
B-7024 Ciply<br/>
Belgium
</p>
<p>All rights reserved.</p>
<a href="http://www.jms2win.com">www.jms2win.com</a>
<?php
   	$model =& $this->getModel( 'registration', 'Edwin2WinModel' );
   	// If not registered
		if ( !$model->isRegistered()) {
      	$view    =& $this->getView(  'registration', '', 'Edwin2WinView');
      	$view->setModel( $model, true );
      	$view->registrationButton();
		}
		else {
         $regInfo =  $model->getRegistrationInfo();
         if ( empty( $regInfo) || empty( $regInfo['product_id'])) {}
         else {
            echo '<br/>' . JText::_( 'Product ID') . ' :' . $regInfo['product_id'];;
         }
		}
		$this->addSubmenu(JRequest::getWord('task', 'about'));
	} // End about

   //------------ registered ---------------
   /**
    * @brief When the status is OK, this save the registered informations.
    * This task is called by the redirect url parameters for the registration button.
    */
	function registered()
	{
   	$option = JRequest::getCmd('option');
   	$model   =& $this->getModel( 'registration',     'Edwin2WinModel' );
   	$view    =& $this->getView(  'registration', '', 'Edwin2WinView');
   	$view->setModel( $model, true );
   	$msg = $view->registered( false);
		$this->setRedirect( 'index.php?option=' . $option . '&task=about', $msg);
		$this->addSubmenu(JRequest::getWord('task', 'about'));
	}


   

} // End class
