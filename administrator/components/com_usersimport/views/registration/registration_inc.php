<?php
// Check to ensure this file is included in Joomla!
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) {
	die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
}

// Developement URL
if ( !defined( 'EDWIN2WIN_REGISTRATION_URL')) {
    define( 'EDWIN2WIN_REGISTRATION_URL', 'http://127.0.0.1/mywebs/w2w/index.php?_host_=jms2win.com');
}
