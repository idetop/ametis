<?php defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.utilities.utility.php');

JFactory::getDocument()->addStyleSheet( 'components/com_docmanrefresh/views/refresh/tmpl/default.css');
?>

<script language="javascript" type="text/javascript">
<!--
	function submitbutton(pressbutton) {
			submitform( pressbutton );
	}
//-->
</script>
<script type="text/javascript" language="JavaScript">
function check() {
  return true;
  
  var ext = document.adminForm.logoImage.value;
  if ( ext.length <= 0) {
   return true;
  }
  ext = ext.substring(ext.length-3,ext.length);
  ext = ext.toLowerCase();
  if(ext != 'png') {
    alert('You selected a .'+ext+
          ' file; please select a .png file instead!');
    return false; }
  else
    return true; }
</script>

<div id="usersimport_import">
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" onsubmit="return check();">
<table>
<tbody>
   <tr class="importfile">
      <td class="label"><label for="importfile"><?php echo JText::_( 'USERSIMPORT_FILE2PROCESS' ); ?>:</label></td>
      <td><input class="inputbox" type="file" name="importfile" id="importfile" size="70" maxlength="150" value="" <?php echo $readonly; ?>/></td>
   </tr>

   <tr class="otherTables">
      <td class="label"><label for="otherTables"><?php echo JText::_( 'USERSIMPORT_OTHERTABLES' ); ?>:</label></td>
      <td><?php echo $this->_getListOtherTables(); ?></td>
   </tr>

   <tr class="importfile">
      <td class="label"><label for="ignoreExtraCols"><?php echo JText::_( 'USERSIMPORT_IGNOREEXTRACOLS' ); ?>:</label></td>
      <td><input type="radio" name="ignoreExtraCols" value="0">No</input>
          <input type="radio" name="ignoreExtraCols" value="1" checked>Yes</input>
      </td>
   </tr>

   <tr class="submit">
      <td></td>
      <td><input class="button" type="submit" value="<?php echo JText::_( 'USERSIMPORT_BUTTON' ); ?>" /></td>
   </tr>
</tbody>
</table>
	<input type="hidden" name="option"        value="com_usersimport" />
	<input type="hidden" name="task"          value="doImport" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>