<?php defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.utilities.utility.php');

JFactory::getDocument()->addStyleSheet( 'components/com_docmanrefresh/views/refresh/tmpl/doimport.css');

// Add the columns 'id', 'error'
$this->columns[] = 'password_clear';
$this->columns[] = 'id';
$this->columns[] = 'error';

?>
<div id="usersimport_doimport">
<table>
<?php if (!empty( $this->columns)) { ?>
   <thead>
   <tr>
<?php    foreach( $this->columns as $column) { ?>
      <th><?php echo $column; ?></th>
<?php    } ?>
   </tr>
   </thead>
<?php } ?>
<?php if ( !empty( $this->rows)) { ?>
   <tbody>
<?php    foreach( $this->rows as $row) { ?>
   <tr>
<?php       if ( !empty( $this->columns)) {
               foreach( $this->columns as $column) {
?>
      <td><?php if ( !empty( $row->$column)) {
                  echo $row->$column;
                  unset( $row->$column);
               }
               else {
                  echo '&nbsp;';
               }
?></td>
<?php
               }
            }
            
            // Now process the additional fields filled during the import
            $otherFields = get_object_vars( $row);
            if ( !empty( $otherFields)) {
?>               
      <td>
<?php          $sep = '';
               foreach( $otherFields as $key => $value) {
                  if ( !empty( $value)) {
                     echo $sep;
                     echo $key .'='.$value;
                     $sep =', ';
                  }
               }
?></td>
<?php
            }
?>            
   </tr>
<?php    } // next row ?>
   </tbody>
<?php } ?>
</table>
	<input type="hidden" name="option"        value="com_usersimport" />
	<input type="hidden" name="task"          value="doImport" />
	<?php echo JHTML::_( 'form.token' ); ?>
</div>