<?php
/**
 * @file       view.php
 * @version    1.2.0
 * @author     Edwin CHERONT     (e.cheront@jms2win.com)
 *             Edwin2Win sprlu   (www.jms2win.com)
 * @copyright  Joomla Multi Sites
 *             Single Joomla! 1.5.x installation using multiple configuration (One for each 'slave' sites).
 *             (C) 2012-2013 Edwin2Win sprlu - all right reserved.
 * @license    This program is free software; you can redistribute it and/or
 *             modify it under the terms of the GNU General Public License
 *             as published by the Free Software Foundation; either version 2
 *             of the License, or (at your option) any later version.
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *             A full text version of the GNU GPL version 2 can be found in the LICENSE.php file.
 * @par History:
 * - 28-FEB-2012 V1.0.0 : File creation
 * - 31-MAY-2012 V1.1.0 : Add ACL options
 * - 12-APR-2013 V1.2.0 : Add the "Other tables" that can be affected with the import
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');


// ===========================================================
//            UsersImportViewImport class
// ===========================================================
class UsersImportViewImport extends JView
{
   // Private members
   var $_formName   = 'Import';
   var $_lcFormName = 'import';


   //------------ Import ---------------
	function Import($tpl=null)
	{
		// JRequest::setVar( 'hidemainmenu', 1 );

		JToolBarHelper::title( JText::_( 'Users Import') .' - '.JText::_( 'Import'), 'config.png' );
		// JToolBarHelper::cancel( 'about');

      if ( version_compare( JVERSION, '1.7') >= 0) {
         // Options button.
         if (JFactory::getUser()->authorise('core.admin', 'com_usersimport')) {
         	JToolBarHelper::preferences('com_usersimport');
         }
      }
      
		JHTML::_('behavior.tooltip');

		parent::display($tpl);
	}

   //------------ _getListOtherTables ---------------
	function _getListOtherTables()
	{
	   $rows  = $this->getModel()->getOtherTables();
	   if ( empty( $rows)) {
	      return '';
	   }
	   
	   $opt = array();
//      $opt[] = JHTML::_('select.option', '[unselected]', '- '.JText::_('Select other tables').' -');
	   foreach( $rows as $key => $value) {
   		$opt[] = JHTML::_('select.option', $key, $value);
	   }
      
		$list = JHTML::_( 'select.genericlist', $opt, "otherTables[]", 
		                  'class="inputbox" multiple="multiple"', 
		                  'value', 'text',
		                  ''
		                );

		return $list;
	}

   //------------ doImport ---------------
	function doImport($tpl=null)
	{
		$app =& JFactory::getApplication();

      $this->setLayout( 'doimport');
		
		// JRequest::setVar( 'hidemainmenu', 1 );

		JToolBarHelper::title( JText::_( 'Users Import') .' - '.JText::_( 'Do Import'), 'config.png' );
		JToolBarHelper::cancel( 'import');

	   $model = $this->getModel();

		// If there is not image to upload.
/*		$file = $this->_getUploadFile();
		if ( empty( $file)) {
		   return JText::_( 'Please enter a file name');
		}
*/

		$importFile =  JRequest::getVar( 'importfile', array(), 'files','array');
		if(empty( $importFile['name'])){
		   return JText::_( 'Please enter a file name');
		}

		$uploadFolder = JFactory::getConfig()->get( 'tmp_path', JPATH_SITE.DS.'tmp');
		$uploadFolder = trim($uploadFolder,DS.' ').DS;
		$uploadPath = JPath::clean( $uploadFolder);
		if(!is_writable($uploadPath)){
			@chmod($uploadPath,'0755');
			if(!is_writable($uploadPath)){
				$app->enqueueMessage(JText::sprintf( 'WRITABLE_FOLDER',$uploadPath), 'notice');
			}
		}
		
		$attachment = null;
		$attachment->filename = strtolower( JFile::makeSafe($importFile['name']));
		$attachment->size = $importFile['size'];
/*
		if(!preg_match('#\.('.str_replace(array(',','.'),array('|','\.'),$config->get('allowedfiles')).')$#Ui',$attachment->filename,$extension) || preg_match('#\.(php.?|.?htm.?|pl|py|jsp|asp|sh|cgi)$#Ui',$attachment->filename)){
			$app->enqueueMessage(JText::sprintf( 'ACCEPTED_TYPE',substr($attachment->filename,strrpos($attachment->filename,'.')+1),$config->get('allowedfiles')), 'notice');
			return false;
		}
		$attachment->filename = str_replace(array('.',' '),'_',substr($attachment->filename,0,strpos($attachment->filename,$extension[0]))).$extension[0];
*/
		if(!JFile::upload($importFile['tmp_name'], $uploadPath . $attachment->filename)){
			if ( !move_uploaded_file($importFile['tmp_name'], $uploadPath . $attachment->filename)) {
				$app->enqueueMessage(JText::sprintf( 'FAIL_UPLOAD',$importFile['tmp_name'],$uploadPath . $attachment->filename), 'error');
				return;
			}
		}
	   
	   $results = $model->doImport( $uploadPath . $attachment->filename);

		// Assign view variable with will be used by the template
		$this->assignRef('columns',   $model->getColumns());
		$this->assignRef('errors',    $model->getImportErrors());
		$this->assignRef('rows',      $model->getImportUsers());

		JHTML::_('behavior.tooltip');

		parent::display($tpl);
	}


   //------------ _getUploadFile ---------------
   function _getUploadFile( $fieldName= 'uploadFile', $uploadTypes=array())
   {
		// If there is not image to upload.
		if ( empty( $_FILES[$fieldName]['tmp_name'])) {
		   return false;
		}
		
      // If the type of the file must be checked.
      $isValidType = true;
      if ( !empty( $uploadTypes)) {
         $isValidType = false;
         $type = $_FILES[$fieldName]['type'];
         $uploadTypes = (array)$uploadTypes;
         foreach( $uploadTypes as $uploadType) {
            $pos = strpos( $type, $uploadType);
            if ( $pos === false) {}
            else {
               $isValidType = true;
               break;
            }
         }
      }
      // If is a valid file type
      if ( isValidType) {
         return $_FILES[$fieldName]['tmp_name'];
      }
      
      return false;
   }

} // End class
