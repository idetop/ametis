<?php
/**
 * @file       admin.usersimport.php
 * @version    1.1.0
 * @author     Edwin CHERONT     (info@jms2win.com)
 *             Edwin2Win sprlu   (www.jms2win.com)
 * @copyright  Jms Multi Sites
 *             With a single Joomla! 1.5.x, create as many joomla configuration as you have sites.
 *             (C) 2008-2011 Edwin2Win sprlu - all right reserved.
 * @license    This program is free software; you can redistribute it and/or
 *             modify it under the terms of the GNU General Public License
 *             as published by the Free Software Foundation; either version 2
 *             of the License, or (at your option) any later version.
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *             A full text version of the GNU GPL version 2 can be found in the LICENSE.php file.
 * @par History:
 * - V1.0.0 03-MAY-2012: Initial version
 * - V1.1.0 31-MAY-2012: Add ACL options
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if ( version_compare( JVERSION, '1.7') >= 0) { 
   // Access check.
   if (!JFactory::getUser()->authorise('core.manage', 'com_usersimport')) {
   	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
   }
}

if ( !defined( 'MULTISITES_MANIFEST_FILENAME')) {
   // If Joomla 1.6
   if ( file_exists( dirname( __FILE__).DS.'extension.xml')) {
      define( 'MULTISITES_MANIFEST_FILENAME', 'extension.xml');
   }
   else {
      // 1.6 based on extension name
      $extname = substr( basename( dirname( __FILE__)), 4);
      if ( file_exists( dirname( __FILE__).DS.$extname.'.xml')) {
         define( 'MULTISITES_MANIFEST_FILENAME', $extname.'.xml');
      }
      // If Joomla 1.5
      else {
         define( 'MULTISITES_MANIFEST_FILENAME', 'install.xml');
      }
   }
}



require_once( JPATH_COMPONENT.DS.'controller.php' );

$controller = new UsersImportController( array('default_task' => 'import') );
$controller->execute( JRequest::getCmd( 'task' ) );
$controller->redirect();