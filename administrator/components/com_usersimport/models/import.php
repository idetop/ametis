<?php
/**
 * @file       import.php
 * @version    1.2.0
 * @author     Edwin CHERONT     (e.cheront@jms2win.com)
 *             Edwin2Win sprlu   (www.jms2win.com)
 * @copyright  Joomla Multi Sites
 *             Single Joomla! 1.5.x installation using multiple configuration (One for each 'slave' sites).
 *             (C) 2008-2013 Edwin2Win sprlu - all right reserved.
 * @license    This program is free software; you can redistribute it and/or
 *             modify it under the terms of the GNU General Public License
 *             as published by the Free Software Foundation; either version 2
 *             of the License, or (at your option) any later version.
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *             A full text version of the GNU GPL version 2 can be found in the LICENSE.php file.
 * @par History:
 * - V1.0.0 11-MAR-2012: File creation
 * - V1.1.0 29-APR-2012: Refresh the deleted files and categories
 *                       Write operations into logfile in ascii mode.
 * - V1.2.0 12-APR-2013: Add the possibility to import hihashop address and hikamarket vendor.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );
jimport( 'joomla.filesystem.path');
jimport( 'joomla.filesystem.file');


class FakeSubject
{
   function attach( $fake) {}
}

// ===========================================================
//             UsersImportModelRefresh  class
// ===========================================================
class UsersImportModelImport extends JModel
{
	// Private members
	var $_modelName = 'import';

	var $importErrors = array();
	var $importUsers  = array();
	var $otherTables  = array();
	var $saveOtherTables = array();
	
	function getOtherTables()  { return $this->otherTables; }
	function getColumns()      { return $this->columns; }
	function getImportErrors() { return $this->importErrors; }
	function getImportUsers()  { return $this->importUsers; }
	
   //------------ Constructor ---------------
	public function __construct($config = array())
	{
	   parent::__construct();
	   $this->db = JFactory::getDBO();
	   $this->_initOtherTables();
	}

   //------------ _initOtherTables ---------------
	protected function _initOtherTables()
	{
	   $tables = array( 'HikaShop Address' => '#__hikashop_address',
//	                    'HikaMarket Vendor' => '#__hikamarket_vendor'
	                  );
	   $this->userIDFields = array( '#__hikashop_address'  => 'address_user_id',
//	                                '#__hikamarket_vendor' => 'vendor_admin_id'
	                              );
	   
	   if ( !empty( $tables)) {
	      foreach( $tables as $label => $table) {
            $like = str_replace('_' , '\_', $this->db->replacePrefix( $table));
            $query = "SHOW TABLES LIKE '$like'";
            $this->db->setQuery($query);
            if ( $this->db->loadResult() == null) {}
            else {
               $this->otherTables[$table] = $label;
            }
	      }
	   }
	}

   // -------------- getLogFileName ------------------------------
   function getLogFileName() {
      $myFilename = JFactory::getConfig()->get( 'log_path', JPATH_SITE.DS.'logs').DS.'usersimport.log.php';
      return $myFilename;
   }

   // -------------- removelog ------------------------------
   function removelog() {
      $myFilename = $this->getLogFileName();
      if ( !file_exists( $myFilename)){
         return "Log file does not exist";
      }
      else {
         @unlink( $myFilename);
         if ( !file_exists( $myFilename)){
            return "Log file successfully deleted.";
         }
         else {
            return "Unable to delete the log file.";
         }
      }
   }

   //------------ _writeLog ---------------
   /**
    * @brief Write the errors into the log file in ascii mode.
    * @remark: $errors contain UFT8 error message
    *          So each line is decoded before writing into the log file.
    */
   function _writeLog( $errors)
   {
      $myFilename = $this->getLogFileName();
      if ( !file_exists( $myFilename)){
         $handle = fopen( $myFilename, "a+");
         
         // Add a PHP Header with a die() statement to avoid reading from a browser
         // This should protect the file from HACKING and access potential sensitive data
         fwrite($handle, "<?php\r\ndie( 'Access forbidden');\r\n");
      }
      else {
         $handle = fopen( $myFilename, "a+");
      }
      
      if ( is_array( $errors)) {
         // fwrite($handle, '//' . implode( "\n//", $errors) . "\r\n");
         foreach( $errors as $error_line) {
            if ( is_string( $error_line)) {}
            else if ( !is_array( $error_line)) {
                $error_line = get_object_vars( $error_line);
            }
            if ( is_array( $error_line)) {
               $str = ''; $sep = '';
               foreach( $error_line as $key => $value) {
                  $str .= $sep.$key.'='.$value;
                  $sep = ', ';
               }
               $error_line = $str;
            }
            // decode ISO-8859-1
            $error_line = html_entity_decode( $error_line);
            fwrite($handle, '//' . utf8_decode( $error_line) . "\r\n");
         }
      }
      else {
         // decode ISO-8859-1
         $error_line = html_entity_decode( $errors);
         fwrite($handle, '//' . utf8_decode( $error_line) . "\r\n");
      }
      
      fclose($handle);
   }

   //------------ _autoDetectHeader ---------------
   /**
    * @brief Detect automatically the header tyype
    */
	function _autoDetectHeader(){
		$app =& JFactory::getApplication();
		$this->separator = ',';

		// Remove the UFT8 signature
		$this->header = str_replace("\xEF\xBB\xBF","",$this->header);
		
		// Detect the field separator (tab, semi-column, comas)
		$listSeparators = array("\t",';',',');
		foreach($listSeparators as $sep){
			if(strpos($this->header,$sep) !== false){
				$this->separator = $sep;
				break;
			}
		}

		// Extract the field name present in the header
		$this->columns = explode($this->separator,$this->header);
		for($i=count($this->columns)-1;$i>=0;$i--){
			if(strlen($this->columns[$i]) == 0){
				unset($this->columns[$i]);
				$this->removeSep++;
			}
		}

		// Extract the field name in the #__users table
		$columnsTableUsers = $this->db->getTableFields( '#__users');
		$columns           = reset( $columnsTableUsers);

		// If there are other tables to process
		if ( !empty( $_REQUEST['otherTables']) && is_array( $_REQUEST['otherTables'])) {
		   foreach( $_REQUEST['otherTables'] as $tableName) {
		      $table = (string) preg_replace('/[^A-Z0-9_#\.]/i', '', $tableName);

      		// Extract the field name in the 'other table'
      		$columnsOtherTable = $this->db->getTableFields( $table);
      		if ( !empty( $columnsOtherTable) && is_array( $columnsOtherTable)) {
      		   $this->saveOtherTables[] = $table;

            	$colOther = reset( $columnsOtherTable);
      		   $columns = array_merge( $columns, $colOther);
      		}
		   }
		}
		
		foreach($this->columns as $i => $oneColumn){
			$this->columns[$i] = strtolower(trim($oneColumn,'\'" '));
			if(!isset($columns[$this->columns[$i]])){
			   if ( $_REQUEST['ignoreExtraCols'] == 1) {}
			   else {
			      $app->enqueueMessage(JText::sprintf('IMPORT_ERROR_FIELD',$this->columns[$i],implode(' | ',array_diff(array_keys($columns),array('username','name','email')))),'error');
   				return false;
			   }
			}
		}
		if(!in_array('email',$this->columns)) return false;
		return true;
	}

   //------------ _db_recExists ---------------
   /**
    * @param keys is either an object or an array
    */
	function _db_recExists( $table, $keys)
	{
		// Initialise variables.
		$fields = array();
		$values = array();

	   if ( JFile::exists( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_multisites' .DS. 'classes' .DS. 'multisitesdb.php')) {
         require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_multisites' .DS. 'classes' .DS. 'multisitesdb.php');
	   }


	   // When JMS is present
	   if ( class_exists( 'MultisitesDatabase')) {
	      // If the table is a view
   	   if ( MultisitesDatabase::_isView( $this->db, $table)) {
   	      // Retreive its physical location
   	     $table = MultisitesDatabase::getViewFrom( $this->db, $table, false, true);
   	   }
	   }


		// Create the base insert statement.
		$columns   = reset( $this->db->getTableFields( $table));
		$statement = 'SELECT * FROM ' . $table . ' WHERE ';

		// Iterate over the object variables to build the query fields and values.
		$where = '';
		$sep = '';
		if ( !is_array( $keys)) {
		   $keys = get_object_vars($keys);
		}
		foreach ( $keys as $k => $v)
		{
			// Only process non-null scalars and the column must exists
			if ( !isset( $columns[$k]) or is_array($v) or is_object($v) or $v === null)
			{
				continue;
			}

			// Ignore any internal fields.
			if ($k[0] == '_')
			{
				continue;
			}

			// Put the where values..
			$where .= $sep . $this->db->quoteName($k) .'='. $this->_db->quote($v);
			$sep = ' AND ';
		}

		// Set the query and execute the insert.
		$this->db->setQuery( $statement.$where, 0, 1);
		$obj = $this->db->loadObject();
		if (!$obj)
		{
			return false;
		}
		
		return $obj;
	}

   //------------ _addUserInMultisitesUserSite ---------------
   /**
    * @param keys is either an object or an array
    */
	function _addUserInMultisitesUserSite( $user_id)
	{
	   if ( JFile::exists( JPATH_PLUGINS.DS.'system'.DS.'multisitesusersite' .DS. 'multisitesusersite.php')) {
         require_once( JPATH_PLUGINS.DS.'system'.DS.'multisitesusersite' .DS. 'multisitesusersite.php');
	   }
	   if ( JFile::exists( JPATH_PLUGINS.DS.'system'.DS. 'multisitesusersite.php')) {
         require_once( JPATH_PLUGINS.DS.'system'.DS. 'multisitesusersite.php');
	   }
	   
	   if ( !class_exists( 'plgSystemMultisitesUserSite')) {
	      return;
	   }
	   
	   // Fake subject
	   $fake = new FakeSubject();
	   $plugin = new plgSystemMultisitesUserSite( $fake, array());
	   
	   $plugin->onUserAfterSave( array( 'id' => $user_id), false, '', '');
	}
	
   //------------ _saveUser---------------
   /**
    * @brief Register the user
    * When ID is not present, this add a record.
    * Otherwise, this update the record
    */
	function _saveUser( &$data, &$errors)
	{
	   if ( !empty( $data['password']) && $data['password']=='<reset>') {
	      // Generate a new password
			$data['password'] = JUserHelper::genRandomPassword();
	   }
	   if ( !empty( $data['password'])) {
			$data['password2'] = $data['password'];
	   }
	   
		$user = new JUser;
		
		// If an ID is present,
		if ( !empty( $data['id'])) {
		   // Retreive the record
		   $user->load( $data['id']);
		}
		// Bind the data.
		if (!$user->bind( $data)) {
		   $data['error'] = JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError());;
			$errors[]      = array_merge( array( 'ERR_001' => ''), $data);
			return false;
		}

		// Load the users plugin group.
		JPluginHelper::importPlugin('user');

		// Store the data.
		if (!$user->save()) {
			$data['error'] = $user->getError();
			$errors[]      = array_merge( array( 'ERR_002' => ''), $data);
			return false;
		}

		$data['id'] = $user->get( 'id');
		$data['password_clear'] = $user->get( 'password_clear');
		
		return true;
	}

   //------------ _saveInTable ---------------
   /**
    * @brief Save the data into a table
    */
	function _saveInTable( $table, &$data, &$errors)
	{
	   // Prepare the record to insert (or update)
		$columns   = reset( $this->db->getTableFields( $table));
      $row = new stdClass();
      foreach( $data as $k => $value) {
         // If the column exists
         if ( isset( $columns[$k])) {
            $row->$k = $value;
         }
      }

	   // If a "user id" key is provided
	   if ( !empty( $this->userIDFields) && $this->userIDFields[$table]) {
   	   $obj = $this->_db_recExists( $table, array( $this->userIDFields[$table] => $data[$this->userIDFields[$table]]));
   	   // If the record does not exists
   	   if ( $obj === false) {
         	if ( !$this->db->insertObject( $table, $row, $this->userIDFields[$table])) {
   		      $errors[] = JText::_( 'Unable to insert record in table')."[$table]" . implode( ', ', get_object_vars($row));
         	}
   	   }
   	   // When the "user" is already present
   	   else {
   	      // Update the record
         	if ( !$this->db->updateObject( $table, $row, $this->userIDFields[$table])) {
   		      $errors[] = JText::_( 'Unable to update the record in table')."[$table]" . implode( ', ', get_object_vars($row));
         	}
   	   }
	   }
	   // Otherwise, use all field columns
	   else {
   	   $obj = $this->_db_recExists( $table, $data);
   	   // If the record does not exists
   	   if ( $obj === false) {
         	if ( !$this->db->insertObject( $table, $row)) {
   		      $errors[] = JText::_( 'Unable to insert record in table')."[$table]" . implode( ', ', get_object_vars($row));
         	}
   	   }
   	   // otherwise,
   	   else {
   	      // Do nothing as ALL the columns are already present (record found).
   	   }
	   }
	}


   //------------ _setOtherField_hikashop_address ---------------
   /**
    * @brief Retreive the Hikashop user ID
    */
	function _setOtherField_hikashop_address( $field, &$data)
	{
      $query = "SELECT user_id FROM #__hikashop_user WHERE user_cms_id=".$data['id'];
      $this->db->setQuery($query);
	   $hika_user_id = $this->db->loadResult();
	   if ( !empty( $hika_user_id)) {
	      $data[$field] = $hika_user_id;
	   }
	}


   //------------ _setOtherField ---------------
   /**
    * @brief Process the user
    */
	function _setOtherField( $table, $field, &$data)
	{
	   // If a specific function is present
	   $fn = '_setOtherField_'.str_replace( '#__','', $table);
	   if ( method_exists( $this, $fn)) {
	      $this->$fn( $field, $data);
	   }
	   // Otherwise,
	   else {
	      // Assign it with the Joomla user 'id'
	      $data[$field] = $data['id'];
	   }
	}


   //------------ _importUsers ---------------
   /**
    * @brief Process the user
    */
	function _importUsers()
	{
	   $errors = array();
		if( empty( $this->importUsers)) {
		   return true;
		}
		
      include_once( JPATH_SITE.'/components/com_users/models/registration.php');

		// For each users
		foreach( $this->importUsers as $i => $row) {
   		$usermodel = new UsersModelRegistration();
   		$data = (array)$usermodel->getData();
		   
   		// Set the User Parameters based on the row fields
   		$vars = get_object_vars( $row);
			foreach ($vars as $key => $value)
			{
			   $data[$key] = $value;
			}
		   
   		// Prepare the data for the user object.
   		if ( !empty( $data['email1']))      { $data['email']		= $data['email1']; }
   		if ( !empty( $data['password1']))   { $data['password']	= $data['password1']; }
   
   
         // ------ Verify the email address ----
   	   if ( !empty( $data['email'])) {
   	      $email = $data['email'];
   	   }
		   
		   // If the username is present
		   if ( !empty( $data['username'])) {
   		   // If record is NOT present
   		   $obj = $this->_db_recExists( '#__users', array( 'username' => $data['username'])); 
   		   if ( $obj === false) {
   		   }
   		   // record exist
   		   else {
   		      // Update the ID
   		      $data['id'] = $obj->id;
   		      
   		      // In case where "multisites user site" is present, verify that an entry exists for this user to ensure that the update is allowed
   		      $this->_addUserInMultisitesUserSite( $obj->id);
   		   }
   		   // Save the new data
		      $this->_saveUser( $data, $errors);
		      
	         // Copy results into the object (row)
	         if ( !empty( $data['id']))              { $this->importUsers[$i]->id = $data['id']; 
	                                                   if ( !empty( $this->userIDFields) && is_array( $this->userIDFields)) {
	                                                      foreach( $this->userIDFields as $table => $field) {
   	                                                      if ( !isset( $data[$field])) {
   	                                                         $this->_setOtherField( $table, $field, $data);
   	                                                      }
	                                                      }
	                                                   }
	                                                 }
	         if ( !empty( $data['password_clear']))  { $this->importUsers[$i]->password_clear = $data['password_clear']; }
	         if ( !empty( $data['error']))           { $this->importUsers[$i]->error = $data['error']; }

      		// When there are additional save functions to process
      		if ( !empty( $this->saveOtherTables)) {
      		   // Call them to populate other tables
      		   foreach( $this->saveOtherTables as $table) {
      		      $this->_saveInTable( $table, $data, $errors);
      		   }
      		}
		   }
		   else {
		      $errors[] = JText::_( 'username field is missing') . implode( ', ', $data);
		   }

		}
		
		if ( !empty( $errors)) {
		   $this->importErrors = array_merge( $this->importErrors, $errors);
		   return false;
		}
		return true;
	}


   //------------ _handleContent ---------------
   /**
    * @brief Handle the content of the file
    */
	function _handleContent(&$contentFile)
	{
		$success = true;
		$this->importErrors = array();
		$this->importUsers  = array();

		$app =& JFactory::getApplication();

		// Normalize the end of line
		$contentFile = str_replace(array("\r\n","\r"),"\n",$contentFile);
		
		// extract the lines
		$importLines = explode("\n", $contentFile);
		
		$this->rows = array();

		// Search for the header line (the first non empty line
		$i = 0;
		$this->header = '';
		while(empty($this->header)){
			$this->header = trim($importLines[$i]);
			$i++;
		}
		
		// Detect the file type (csv, tab) and extract the column names
		if(!$this->_autoDetectHeader()){
			$app->enqueueMessage(JText::sprintf('IMPORT_HEADER',$this->header),'error');
			$app->enqueueMessage(JText::_('IMPORT_USER'),'error');
			$app->enqueueMessage(JText::_('IMPORT_EXAMPLE'),'error');
			return false;
		}
		
		// For each lines
		$numberColumns = count($this->columns);
		while (isset($importLines[$i])) {
		   // Extract the fields data
			$data = explode($this->separator,rtrim(trim($importLines[$i]),$this->separator));
			if(!empty($this->removeSep)){
				for($b = $numberColumns+$this->removeSep-1;$b >= $numberColumns;$b-- ){
					if(isset($data[$b]) AND strlen($data[$b])==0){
						unset($data[$b]);
					}
				}
			}
			$i++;
			if(empty($importLines[$i-1])) continue;
			
			$this->totalTry++;
			if(count($data) > $numberColumns){
				$copy = $data;
				foreach($copy as $oneelem => $oneval){
					if(!empty($oneval[0]) AND $oneval[0] == '"' AND $oneval[strlen($oneval)-1] != '"' AND isset($copy[$oneelem+1]) AND $copy[$oneelem+1][strlen($copy[$oneelem+1])-1] == '"'){
						$data[$oneelem] = $copy[$oneelem].$this->separator.$copy[$oneelem+1];
						unset($data[$oneelem+1]);
					}
				}
				$data = array_values($data);
			}
			if(count($data) < $numberColumns){
				for($a=count($data);$a<$numberColumns;$a++){
					$data[$a] = '';
				}
			}
			if(count($data) != $numberColumns){
				$success = false;
				static $errorcount = 0;
				if(empty($errorcount)){
					$app->enqueueMessage(JText::sprintf('IMPORT_ARGUMENTS',$numberColumns),'error');
				}
				$errorcount++;
				if($errorcount<20){
					$app->enqueueMessage(JText::sprintf('IMPORT_ERRORLINE',$importLines[$i-1]),'notice');
				}elseif($errorcount == 20){
					$app->enqueueMessage('...','notice');
				}
				if($this->totalTry == 1) return false;
				continue;
			}
			
			// Process the data
			$newUser = null;
			foreach($data as $num => $value){
				$field = $this->columns[$num];
				if ( empty( $field)) {
				   continue;
				}
				if ( !empty( $value)) { $newUser->$field = trim($value,'\'" '); }
				else                  { $newUser->$field = ''; }
				if(!empty($this->charsetConvert)){
					// $newUser->$field = $encodingHelper->change($newUser->$field,$this->charsetConvert,'UTF-8');
				}
			}
/*
			$newUser->email = trim(str_replace(array(' ',"\t"),'',$encodingHelper->change($newUser->email,'UTF-8','ISO-8859-1')));
			if(!$userHelper->validEmail($newUser->email)){
				$success = false;
				static $errorcountfail = 0;
				$errorcountfail++;
				if($errorcountfail<20){
					$app->enqueueMessage(JText::sprintf('NOT_VALID_EMAIL',$newUser->email).' | '.($i-1).' : '.$importLines[$i-1],'notice');
				}elseif($errorcountfail == 20){
					$app->enqueueMessage('...','notice');
				}
				continue;
			}
*/
			$this->importUsers[] = $newUser;
		}
		$this->_importUsers( $this->importUsers);
		return $success;
	}


   //------------ doImport ---------------
   /**
    * @brief Perform the import of a file.
    */
   function doImport( $fileName)
   {
      $this->_writeLog( "-------------- " . date("F j, Y, g:i a") . " --------------");



		$contentFile = file_get_contents( $fileName);
		if( !$contentFile) {
//			$app->enqueueMessage(JText::sprintf( 'FAIL_OPEN',$uploadPath . $attachment->filename), 'error');
			return false;
		}

		unlink( $fileName);
		if(empty($this->charsetConvert) && function_exists('mb_check_encoding')){
			if(!mb_check_encoding($contentFile,'UTF-8')){
				if(mb_check_encoding($contentFile,'ISO-8859-1')){
					$this->charsetConvert = 'ISO-8859-1';
				}elseif(mb_check_encoding($contentFile,'ISO-8859-2')){
					$this->charsetConvert = 'ISO-8859-2';
				}elseif(mb_check_encoding($contentFile,'Windows-1252')){
					$this->charsetConvert = 'Windows-1252';
				}
			}
		}
		$result = $this->_handleContent($contentFile);

      // Write imported records
      $this->_writeLog( "--- Users imported ---");
      if ( empty( $this->importUsers)) {
//         $this->_writeLog( JText::_( 'USERSIMPORT_SUCCESSFULL'));
      }
      else {
         $this->_writeLog( $this->importUsers);
      }
      $this->_writeLog( "--- Results ---");
      // Write the error results
      if ( empty( $this->importErrors)) {
         $this->_writeLog( JText::_( 'USERSIMPORT_SUCCESSFULL'));
      }
      else {
         $this->_writeLog( $this->importErrors);
      }
      
      return $result;
   }


} // End class
