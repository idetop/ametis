# Ametis cosmetics
==================

## TODO

## Références et configuration

### Outils de projet
- Versioning : [Git](http://git-scm.com)
- [Editor config](http://editorconfig.org) consistent coding styles between different editors and IDEs

### Back-end
- [Joomla 3.4](https://www.joomla.org) framework / CMS
- Extension e-commerce [HikaShop Business 2.5.0](http://www.hikashop.com/extensions/hika-business.html)
- Extension [AcyMailing](http://www.acyba.com/acymailing/features.html)
- Extension [Falang 2.1.0](http://www.faboba.com/composants/falang/documentation.html)
